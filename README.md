# README #

# Report #
Read the report here: https://drive.google.com/file/d/0B03RkGknWBpNMjJ0em96MmVKM2M/view?usp=sharing


# Compilation #
To compile the client and the server run the 'compile' script.

# Server #
To run the server use the 'server' script.
The server uses two properties files:

###server.properties:###

| Attribute                 | Description                                                                                           |
|---------------------------|----------------------------------------------------------------------------------------|
| port                      | # the port number the server should run on.                                        |
| executionTimeLimit  		| # the time the clients has to finish a task.                                             |
| totalTimeLimit         	| # the time the server should wait for a work to finish.                            |
| replication               | # number of clients calculating the same job.                                       |
| quorum                    | # number of equal answers required for a computation to be valid.      |
| taskHistory               | # number of old jobs to keep for performance evaluation.                    |



###chess.properties:###

| Attribute               | Description                                                                                                |
|-----------------------|--------------------------------------------------------------------------------------------|
| desiredDepth      	| # the depth the clients should search if the time limit doesn't occur first.  |

# Client #
To run the client run the 'client' script.

The client uses one script:

###client.properties:###

| Attribute                 | Description                                                                                          |
|---------------------------|---------------------------------------------------------------------------------------|
| host                      | # the hostname/ip of the server                                                          |
| port                      | # the port the server uses                                                                  |
