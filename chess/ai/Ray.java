package chess.ai;

import java.util.ArrayList;

public class Ray {

    private ArrayList<Move> moves;

    public Ray() {
	moves = new ArrayList<Move>();
    }

    public ArrayList<Move> getMoves() {
	return moves;
    }
}
