package chess.ai;

public class PieceType {

    // order is important for indexing look up table.
    public final static byte BLACK_PAWN = 0;
    public final static byte WHITE_PAWN = 1;
    public final static byte ROOK = 2;
    public final static byte KNIGHT = 3;
    public final static byte BISHOP = 4;
    public final static byte QUEEN = 5;
    public final static byte KING = 6;
    public final static byte PAWN_QUEEN = 7;

    public final static byte BLACK_ROOK = 14;
    public final static byte BLACK_KNIGHT = 21;
    public final static byte BLACK_BISHOP = 28;
    public final static byte BLACK_QUEEN = 35;
    public final static byte BLACK_KING = 42;
    public final static byte BLACK_PAWN_QUEEN = 49;

    public final static byte WHITE_ROOK = 8;
    public final static byte WHITE_KNIGHT = 16;
    public final static byte WHITE_BISHOP = 24;
    public final static byte WHITE_QUEEN = 32;
    public final static byte WHITE_KING = 40;
    public final static byte WHITE_PAWN_QUEEN = 48;

    public final static byte NONE = 100;

    public final static byte BLACK = 101;
    public final static byte WHITE = 102;

    public static boolean isWhite(byte piece) {
	if (piece == WHITE_PAWN) {
	    return true;
	} else if (piece == BLACK_PAWN) {
	    return false;
	}
	return (piece % 8 == 0);
    }

    public static boolean isBlack(byte piece) {
	if (piece == BLACK_PAWN) {
	    return true;
	} else if (piece == WHITE_PAWN) {
	    return false;
	}
	return (piece % 7 == 0);
    }

    public static byte getPieceType(byte piece) {
	if (piece == BLACK_PAWN) {
	    return BLACK_PAWN;
	} else if (piece == WHITE_PAWN) {
	    return WHITE_PAWN;
	} else if (piece == BLACK_PAWN_QUEEN) {
	    return PAWN_QUEEN;
	} else if (piece == WHITE_PAWN_QUEEN) {
	    return PAWN_QUEEN;
	} else if (piece == BLACK_ROOK) {
	    return ROOK;
	} else if (piece == BLACK_KNIGHT) {
	    return KNIGHT;
	} else if (piece == BLACK_BISHOP) {
	    return BISHOP;
	} else if (piece == BLACK_QUEEN) {
	    return QUEEN;
	} else if (piece == BLACK_KING) {
	    return KING;
	} else if (piece == WHITE_ROOK) {
	    return ROOK;
	} else if (piece == WHITE_KNIGHT) {
	    return KNIGHT;
	} else if (piece == WHITE_BISHOP) {
	    return BISHOP;
	} else if (piece == WHITE_QUEEN) {
	    return QUEEN;
	} else if (piece == WHITE_KING) {
	    return KING;
	} else {
	    return -1;
	}
    }

    public static String toString(byte piece) {
	switch (piece) {
	case BLACK_PAWN:
	    return "BP";
	case BLACK_KING:
	    return "BK";
	case BLACK_QUEEN:
	    return "BQ";
	case BLACK_BISHOP:
	    return "BB";
	case BLACK_KNIGHT:
	    return "Bk";
	case BLACK_ROOK:
	    return "BR";
	case BLACK_PAWN_QUEEN:
	    return "BPQ";
	case WHITE_PAWN:
	    return "WP";
	case WHITE_KING:
	    return "WK";
	case WHITE_QUEEN:
	    return "WQ";
	case WHITE_BISHOP:
	    return "WB";
	case WHITE_KNIGHT:
	    return "Wk";
	case WHITE_ROOK:
	    return "WR";
	case WHITE_PAWN_QUEEN:
	    return "WPQ";
	default:
	    return "X";
	}

    }

}
