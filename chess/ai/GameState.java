package chess.ai;

import java.io.Serializable;

public class GameState implements Serializable {

    private static final long serialVersionUID = -7173888378335561507L;
    // PAWNS 0-7
    // ROOK 8-9
    // KNIGHT 10-11
    // BIHSOP 12-13
    // QUEEN 14
    // KING 15
    // PAWNQUEEN 16-23

    private byte[] board;
    private byte[] whitePositions;
    private byte[] blackPositions;

    private boolean blackRightCastlingPossible;
    private boolean blackLeftCastlingPossible;
    private boolean whiteRightCastlingPossible;
    private boolean whiteLeftCastlingPossible;

    private byte aiColor;
    private byte currentColor;

    public GameState(byte aiColor) {
	this.aiColor = aiColor;
	this.currentColor = aiColor;
	this.board = ChessBoardBuilder.buildChessBoard();
	this.whitePositions = ChessBoardBuilder.buildWhitePositions();
	this.blackPositions = ChessBoardBuilder.buildBlackPositions();
	blackRightCastlingPossible = true;
	blackLeftCastlingPossible = true;
	whiteRightCastlingPossible = true;
	whiteLeftCastlingPossible = true;
    }

    public GameState(byte aiColor, byte currentColor, byte[] board,
	    byte[] whitePositions, byte[] blackPositions,
	    boolean blackRightCastlingPossible,
	    boolean blackLeftCastlingPossible,
	    boolean whiteRightCastlingPossbile,
	    boolean whiteLeftCastlingPossbile) {
	this.aiColor = aiColor;
	this.currentColor = currentColor;
	this.board = board;
	this.whitePositions = whitePositions;
	this.blackPositions = blackPositions;
	this.blackRightCastlingPossible = blackRightCastlingPossible;
	this.blackLeftCastlingPossible = blackLeftCastlingPossible;
	this.whiteRightCastlingPossible = whiteRightCastlingPossbile;
	this.whiteLeftCastlingPossible = whiteLeftCastlingPossbile;
    }

    public byte[] getWhitePositions() {
	return whitePositions;
    }

    public byte[] getBlackPositions() {
	return blackPositions;
    }

    public byte[] getBoard() {
	return board;
    }

    public byte getAiColor() {
	return aiColor;
    }

    public byte getCurrentColor() {
	return currentColor;
    }

    public void nextPlayer() {
	if (currentColor == PieceType.BLACK) {
	    currentColor = PieceType.WHITE;
	} else {
	    currentColor = PieceType.BLACK;
	}
    }

    public boolean doMove(Move move) {
	byte pieceToMove = board[move.getFrom()];
	byte pieceAtDestination = board[move.getTo()];
	board[move.getFrom()] = PieceType.NONE;
	board[move.getTo()] = pieceToMove;
	if (findAndReplace(pieceToMove, move.getFrom(), move.getTo())) {
	    if (pieceAtDestination != PieceType.NONE) {
		return findAndReplace(pieceAtDestination, move.getTo(),
			Square.OUTOFDIMENSION);
	    }
	    return true;
	} else {
	    return false;
	}
	// nextPlayer();

    }

    public GameState clone() {
	return new GameState(aiColor, currentColor, board.clone(),
		whitePositions.clone(), blackPositions.clone(),
		blackRightCastlingPossible, blackLeftCastlingPossible,
		whiteRightCastlingPossible, whiteLeftCastlingPossible);
    }

    private boolean findAndReplace(byte piece, byte square, byte replacement) {
	boolean validMove = true;
	switch (PieceType.getPieceType(piece)) {
	case PieceType.KING:

	    if (PieceType.isBlack(piece)) {
		// Castling
		if (square == Square.E8 && replacement == Square.G8) {
		    if (blackRightCastlingPossible && board[Square.F8] == PieceType.NONE) {
			blackPositions[9] = Square.F8;
			board[Square.H8] = PieceType.NONE;
			board[Square.F8] = PieceType.BLACK_ROOK;
			blackPositions[15] = replacement;
			blackRightCastlingPossible = false;
			blackLeftCastlingPossible = false;
		    } else {
			validMove = false;
		    }
		} else if (square == Square.E8 && replacement == Square.C8) {
		    if (blackLeftCastlingPossible && board[Square.B8] == PieceType.NONE && board[Square.D8] == PieceType.NONE) {
			blackPositions[8] = Square.D8;
			board[Square.A8] = PieceType.NONE;
			board[Square.D8] = PieceType.BLACK_ROOK;
			blackPositions[15] = replacement;
			blackRightCastlingPossible = false;
			blackLeftCastlingPossible = false;
		    } else {
			validMove = false;
		    }
		} else {
		    blackPositions[15] = replacement;
		    blackRightCastlingPossible = false;
		    blackLeftCastlingPossible = false;
		}
	    } else {
		// Castling
		if (square == Square.E1 && replacement == Square.G1) {
		    if (whiteRightCastlingPossible && board[Square.F1] == PieceType.NONE) {
			whitePositions[9] = Square.F1;
			board[Square.H1] = PieceType.NONE;
			board[Square.F1] = PieceType.WHITE_ROOK;
			whitePositions[15] = replacement;
			whiteRightCastlingPossible = false;
			whiteLeftCastlingPossible = false;
		    } else {
			validMove = false;
		    }
		} else if (square == Square.E1 && replacement == Square.C1) {
		    if (whiteLeftCastlingPossible && board[Square.B1] == PieceType.NONE && board[Square.D1] == PieceType.NONE) {
			whitePositions[8] = Square.D1;
			board[Square.A1] = PieceType.NONE;
			board[Square.D1] = PieceType.WHITE_ROOK;
			whitePositions[15] = replacement;
			whiteRightCastlingPossible = false;
			whiteLeftCastlingPossible = false;
		    } else {
			validMove = false;
		    }
		} else {
		    whitePositions[15] = replacement;
		    whiteRightCastlingPossible = false;
		    whiteLeftCastlingPossible = false;
		}
	    }

	    break;
	case PieceType.QUEEN:
	    if (PieceType.isBlack(piece)) {
		blackPositions[14] = replacement;
	    } else {
		whitePositions[14] = replacement;
	    }
	    break;

	case PieceType.BISHOP:
	    if (PieceType.isBlack(piece)) {
		for (int i = 12; i < 14; i++) {
		    if (blackPositions[i] == square) {
			blackPositions[i] = replacement;
			break;
		    }
		}
	    } else {
		for (int i = 12; i < 14; i++) {
		    if (whitePositions[i] == square) {
			whitePositions[i] = replacement;
			break;
		    }
		}
	    }

	    break;

	case PieceType.KNIGHT:
	    if (PieceType.isBlack(piece)) {
		for (int i = 10; i < 12; i++) {
		    if (blackPositions[i] == square) {
			blackPositions[i] = replacement;
			break;
		    }
		}
	    } else {
		for (int i = 10; i < 12; i++) {
		    if (whitePositions[i] == square) {
			whitePositions[i] = replacement;
			break;
		    }
		}
	    }

	    break;

	case PieceType.ROOK:
	    if (PieceType.isBlack(piece)) {
		for (int i = 8; i < 10; i++) {
		    if (blackPositions[i] == square) {
			blackPositions[i] = replacement;
			if (i == 9) {
			    blackRightCastlingPossible = false;
			} else {
			    blackLeftCastlingPossible = false;
			}
			break;
		    }
		}
	    } else {
		for (int i = 8; i < 10; i++) {
		    if (whitePositions[i] == square) {
			whitePositions[i] = replacement;
			if (i == 9) {
			    whiteRightCastlingPossible = false;
			} else {
			    whiteLeftCastlingPossible = false;
			}
			break;
		    }
		}
	    }

	    break;

	case PieceType.BLACK_PAWN:
	    for (int i = 0; i < 8; i++) {
		if (blackPositions[i] == square) {
		    if (replacement > 0 && replacement < 9) {
			// Turn pawn into queen
			blackPositions[i] = Square.OUTOFDIMENSION;
			blackPositions[i + 16] = replacement;
			board[replacement] = PieceType.BLACK_PAWN_QUEEN;
		    } else {
			blackPositions[i] = replacement;
		    }
		    break;
		}
	    }

	    break;

	case PieceType.PAWN_QUEEN:
	    if (PieceType.isBlack(piece)) {
		for (int i = 16; i < 24; i++) {
		    if (blackPositions[i] == square) {
			blackPositions[i] = replacement;
			break;
		    }
		}
	    } else {
		for (int i = 16; i < 24; i++) {
		    if (whitePositions[i] == square) {
			whitePositions[i] = replacement;
			break;
		    }
		}
	    }

	    break;

	case PieceType.WHITE_PAWN:
	    for (int i = 0; i < 8; i++) {
		if (whitePositions[i] == square) {
		    if (replacement > 70 && replacement < 79) {
			// Turn pawn into queen
			whitePositions[i] = Square.OUTOFDIMENSION;
			whitePositions[i + 16] = replacement;
			board[replacement] = PieceType.WHITE_PAWN_QUEEN;
		    } else {
			whitePositions[i] = replacement;
		    }
		    break;
		}
	    }
	    break;

	default:
	    validMove = false;
	    break;

	}
	return validMove;
    }

    // For testing
    @Override
    public String toString() {
	String line = "\ta\tb\tc\td\te\tf\tg\th\n--------------------------------------------------------------------------\n";
	line += "8|\t";
	byte square = Square.A8;
	while (square > 0) {

	    line += PieceType.toString(board[square]) + "\t";
	    square++;
	    // Skip invalid square
	    if ((square + 1) % 10 == 0) {
		line += "|" + Square.toString((byte) (square - 1)).charAt(1)
			+ "\n";
		square -= 18;
		if (square > 0) {
		    line += Square.toString(square).charAt(1) + "|\t";
		}
	    }
	}
	return line
		+ "--------------------------------------------------------------------------\n\ta\tb\tc\td\te\tf\tg\th\n";
    }

    public boolean bothKingsAlive() {
	return whitePositions[15] != Square.OUTOFDIMENSION && blackPositions[15] != Square.OUTOFDIMENSION;
    }

    // //For testing
    // public void toString(byte black, byte white) {
    // System.out.print("\ta\tb\tc\td\te\tf\tg\th\n--------------------------------------------------------------------------\n");
    // System.out.print("8|\t");
    // byte square = Square.A8;
    // while(square > 0) {
    //
    // if(square == black || square == white) {
    // System.out.print((char)27 + "[1m" + PieceType.toString(board[square]) +
    // (char)27 + "[0m\t");
    // } else {
    // System.out.print(PieceType.toString(board[square]) + "\t");
    // }
    // square++;
    // //Skip invalid square
    // if((square+1) % 10 == 0) {
    // System.out.println("|" + Square.toString((byte)(square-1)).charAt(1) +
    // "\n");
    // square -= 18;
    // if(square > 0) {
    // System.out.print(Square.toString(square).charAt(1) + "|\t");
    // }
    // }
    // }
    // System.out.print("--------------------------------------------------------------------------\n\ta\tb\tc\td\te\tf\tg\th\n");
    // }

}
