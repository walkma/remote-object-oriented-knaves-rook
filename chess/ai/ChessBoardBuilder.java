package chess.ai;

public class ChessBoardBuilder {


    public static byte[] buildChessBoard() {
	byte[] board = new byte[80];

	for (int i = Square.A1; i <= Square.H8; i++) {
		board[i] = PieceType.NONE;
	}


	board[Square.A1] = PieceType.WHITE_ROOK;
	board[Square.H1] = PieceType.WHITE_ROOK;
	board[Square.B1] = PieceType.WHITE_KNIGHT;
	board[Square.G1] = PieceType.WHITE_KNIGHT;
	board[Square.C1] = PieceType.WHITE_BISHOP;
	board[Square.F1] = PieceType.WHITE_BISHOP;
	board[Square.D1] = PieceType.WHITE_QUEEN;
	board[Square.E1] = PieceType.WHITE_KING;


	board[Square.A8] = PieceType.BLACK_ROOK;
	board[Square.H8] = PieceType.BLACK_ROOK;
	board[Square.B8] = PieceType.BLACK_KNIGHT;
	board[Square.G8] = PieceType.BLACK_KNIGHT;
	board[Square.C8] = PieceType.BLACK_BISHOP;
	board[Square.F8] = PieceType.BLACK_BISHOP;
	board[Square.D8] = PieceType.BLACK_QUEEN;
	board[Square.E8] = PieceType.BLACK_KING;

	for(int i = Square.A2; i <= Square.H2 ; i++){
	    board[i] = PieceType.WHITE_PAWN;
	}
	for(int i = Square.A7; i <= Square.H7 ; i++){
	    board[i] = PieceType.BLACK_PAWN;
	}
	return board;
    }

    public static byte[] buildRayBoard(){

	byte[] board = new byte[80];

	for(int i = Square.L1; i <= Square.R8; i++){
	    board[i] = Square.INVALID;
	}

	for(int i = Square.A1; i <= Square.H1; i++){
	    board[i] = Square.VALID;
	}
	for(int i = Square.A2; i <= Square.H2; i++){
	    board[i] = Square.VALID;
	}
	for(int i = Square.A3; i <= Square.H3; i++){
	    board[i] = Square.VALID;
	}
	for(int i = Square.A4; i <= Square.H4; i++){
	    board[i] = Square.VALID;
	}
	for(int i = Square.A5; i <= Square.H5; i++){
	    board[i] = Square.VALID;
	}
	for(int i = Square.A6; i <= Square.H6; i++){
	    board[i] = Square.VALID;
	}
	for(int i = Square.A7; i <= Square.H7; i++){
	    board[i] = Square.VALID;
	}
	for(int i = Square.A8; i <= Square.H8; i++){
	    board[i] = Square.VALID;
	}
	return board;
    }

    public static byte[] buildWhitePositions() {
	byte[] whitePositions = new byte[24];

	for(byte i = Square.A2; i <= Square.H2; i++) {
	    whitePositions[i-11] = i;
	}
	whitePositions[8] = Square.A1;
	whitePositions[9] = Square.H1;
	whitePositions[10] = Square.B1;
	whitePositions[11] = Square.G1;
	whitePositions[12] = Square.C1;
	whitePositions[13] = Square.F1;
	whitePositions[14] = Square.D1;
	whitePositions[15] = Square.E1;
	for(int i = 16; i < 24; i++) {
	    whitePositions[i] = Square.OUTOFDIMENSION;
	}

	return whitePositions;
    }

    public static byte[] buildBlackPositions() {
	byte[] blackPositions = new byte[24];

	for(byte i = Square.A7; i <= Square.H7; i++) {
	    blackPositions[i-61] = i;
	}
	blackPositions[8] = Square.A8;
	blackPositions[9] = Square.H8;
	blackPositions[10] = Square.B8;
	blackPositions[11] = Square.G8;
	blackPositions[12] = Square.C8;
	blackPositions[13] = Square.F8;
	blackPositions[14] = Square.D8;
	blackPositions[15] = Square.E8;
	for(int i = 16; i < 24; i++) {
	    blackPositions[i] = Square.OUTOFDIMENSION;
	}

	return blackPositions;
    }

}
