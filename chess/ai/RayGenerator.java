package chess.ai;

import java.util.ArrayList;

public class RayGenerator {

    private byte[] board;

    public RayGenerator() {
	this.board = ChessBoardBuilder.buildRayBoard();
	generateRaysForKingAllSquares();
	generateRaysForRookAllSquares();
	generateRaysForBishopAllSquares();
	generateRaysForQueenAllSquares();
	generateRaysForBlackPawnAllSquares();
	generateRaysForWhitePawnAllSquares();
	generateRaysForKnightAllSquares();
	generateRaysForPawnQueenAllSquares();
    }

    /**
     * KNIGHT, all squares
     */
    private void generateRaysForKnightAllSquares() {
	for (byte i = Square.A1; i <= Square.H8; i++) {

	    Ray[] rays = generateRaysForKnight(i);

	    for (int k = 0; k < rays.length; k++) {
		Table.rayTable[PieceType.KNIGHT][i][k] = rays[k];
	    }

	    // Skip invalid square
	    if ((i + 2) % 10 == 0) {
		i += 2;
	    }
	}
    }

    /**
     * KNIGHT, one square
     */
    private Ray[] generateRaysForKnight(byte square) {

	ArrayList<Ray> arrRay = new ArrayList<Ray>();
	int count = 0;

	// 7
	if (isValidSquare(getSquareNorth(getSquareNorthWest(square)))) {
	    arrRay.add(new Ray());
	    arrRay.get(count)
		    .getMoves()
		    .add(new Move(square,
			    getSquareNorth(getSquareNorthWest(square))));
	    count++;
	}

	if (isValidSquare(getSquareNorth(getSquareNorthEast(square)))) {
	    arrRay.add(new Ray());
	    arrRay.get(count)
		    .getMoves()
		    .add(new Move(square,
			    getSquareNorth(getSquareNorthEast(square))));
	    count++;
	}

	if (isValidSquare(getSquareEast(getSquareNorthEast(square)))) {
	    arrRay.add(new Ray());
	    arrRay.get(count)
		    .getMoves()
		    .add(new Move(square,
			    getSquareEast(getSquareNorthEast(square))));
	    count++;
	}

	if (isValidSquare(getSquareEast(getSquareSouthEast(square)))) {
	    arrRay.add(new Ray());
	    arrRay.get(count)
		    .getMoves()
		    .add(new Move(square,
			    getSquareEast(getSquareSouthEast(square))));
	    count++;
	}

	if (isValidSquare(getSquareSouth(getSquareSouthEast(square)))) {
	    arrRay.add(new Ray());
	    arrRay.get(count)
		    .getMoves()
		    .add(new Move(square,
			    getSquareSouth(getSquareSouthEast(square))));
	    count++;
	}

	if (isValidSquare(getSquareSouth(getSquareSouthWest(square)))) {
	    arrRay.add(new Ray());
	    arrRay.get(count)
		    .getMoves()
		    .add(new Move(square,
			    getSquareSouth(getSquareSouthWest(square))));
	    count++;
	}

	if (isValidSquare(getSquareWest(getSquareSouthWest(square)))) {
	    arrRay.add(new Ray());
	    arrRay.get(count)
		    .getMoves()
		    .add(new Move(square,
			    getSquareWest(getSquareSouthWest(square))));
	    count++;
	}

	if (isValidSquare(getSquareWest(getSquareNorthWest(square)))) {
	    arrRay.add(new Ray());
	    arrRay.get(count)
		    .getMoves()
		    .add(new Move(square,
			    getSquareWest(getSquareNorthWest(square))));
	    count++;
	}

	Ray[] rays = new Ray[count];

	for (int i = 0; i < count; i++) {
	    rays[i] = arrRay.get(i);
	}

	return rays;
    }

    /**
     * WHITE PAWN, all squares
     */
    private void generateRaysForWhitePawnAllSquares() {
	for (byte i = Square.A1; i <= Square.H8; i++) {

	    Ray[] rays = generateRaysForWhitePawn(i);

	    for (int k = 0; k < rays.length; k++) {
		Table.rayTable[PieceType.WHITE_PAWN][i][k] = rays[k];
	    }

	    // Skip invalid square
	    if ((i + 2) % 10 == 0) {
		i += 2;
	    }
	}
    }

    /**
     * WHITE PAWN, one square
     */
    private Ray[] generateRaysForWhitePawn(byte square) {

	ArrayList<Ray> arrRay = new ArrayList<Ray>();
	int count = 0;

	if (isWhitePawnStart(square)
		&& isValidSquare(getSquareNorth(getSquareNorth(square)))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareNorth(square)));
	    arrRay.get(count)
		    .getMoves()
		    .add(new Move(square,
			    getSquareNorth(getSquareNorth(square))));
	    count++;
	} else if (isValidSquare(getSquareNorth(square))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareNorth(square)));
	    count++;
	}

	if (isValidSquare(getSquareNorthWest(square))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareNorthWest(square)));
	    count++;
	}

	if (isValidSquare(getSquareNorthEast(square))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareNorthEast(square)));
	    count++;
	}

	Ray[] rays = new Ray[count];

	for (int i = 0; i < count; i++) {
	    rays[i] = arrRay.get(i);
	}

	return rays;
    }

    /**
     * BLACK PAWN, all squares
     */
    private void generateRaysForBlackPawnAllSquares() {
	for (byte i = Square.A1; i <= Square.H8; i++) {

	    Ray[] rays = generateRaysForBlackPawn(i);

	    for (int k = 0; k < rays.length; k++) {
		Table.rayTable[PieceType.BLACK_PAWN][i][k] = rays[k];
	    }

	    // Skip invalid square
	    if ((i + 2) % 10 == 0) {
		i += 2;
	    }
	}
    }

    /**
     * BLACK PAWN, one square
     */
    private Ray[] generateRaysForBlackPawn(byte square) {

	ArrayList<Ray> arrRay = new ArrayList<Ray>();
	int count = 0;

	if (isBlackPawnStart(square)
		&& isValidSquare(getSquareSouth(getSquareSouth(square)))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareSouth(square)));
	    arrRay.get(count)
		    .getMoves()
		    .add(new Move(square,
			    getSquareSouth(getSquareSouth(square))));
	    count++;
	} else if (isValidSquare(getSquareSouth(square))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareSouth(square)));
	    count++;
	}

	if (isValidSquare(getSquareSouthWest(square))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareSouthWest(square)));
	    count++;
	}

	if (isValidSquare(getSquareSouthEast(square))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareSouthEast(square)));
	    count++;
	}

	Ray[] rays = new Ray[count];

	for (int i = 0; i < count; i++) {
	    rays[i] = arrRay.get(i);
	}

	return rays;
    }

    /**
     * QUEEN, all squares
     */
    private void generateRaysForQueenAllSquares() {
	for (byte i = Square.A1; i <= Square.H8; i++) {

	    Ray[] diagonal = generateRaysForDiagonal(i, PieceType.QUEEN);
	    Ray[] horver = generateRaysForHorizontalVertical(i, PieceType.QUEEN);
	    Ray[] rays = new Ray[diagonal.length + horver.length];
	    System.arraycopy(diagonal, 0, rays, 0, diagonal.length);
	    System.arraycopy(horver, 0, rays, diagonal.length, horver.length);

	    for (int k = 0; k < rays.length; k++) {
		Table.rayTable[PieceType.QUEEN][i][k] = rays[k];
	    }

	    // Skip invalid square
	    if ((i + 2) % 10 == 0) {
		i += 2;
	    }
	}
    }

    /**
     * PAWN_QUEEN, all squares
     */
    private void generateRaysForPawnQueenAllSquares() {
	for (byte i = Square.A1; i <= Square.H8; i++) {

	    Ray[] diagonal = generateRaysForDiagonal(i, PieceType.PAWN_QUEEN);
	    Ray[] horver = generateRaysForHorizontalVertical(i,
		    PieceType.PAWN_QUEEN);
	    Ray[] rays = new Ray[diagonal.length + horver.length];
	    System.arraycopy(diagonal, 0, rays, 0, diagonal.length);
	    System.arraycopy(horver, 0, rays, diagonal.length, horver.length);

	    for (int k = 0; k < rays.length; k++) {
		Table.rayTable[PieceType.PAWN_QUEEN][i][k] = rays[k];
	    }

	    // Skip invalid square
	    if ((i + 2) % 10 == 0) {
		i += 2;
	    }
	}
    }

    /**
     * BISHOP, all squares
     */
    private void generateRaysForBishopAllSquares() {
	for (byte i = Square.A1; i <= Square.H8; i++) {

	    Ray[] rays = generateRaysForDiagonal(i, PieceType.BISHOP);
	    for (int k = 0; k < rays.length; k++) {
		Table.rayTable[PieceType.BISHOP][i][k] = rays[k];
	    }

	    // Skip invalid square
	    if ((i + 2) % 10 == 0) {
		i += 2;
	    }
	}
    }

    /**
     * DIAGONAL, one square
     */
    private Ray[] generateRaysForDiagonal(byte square, byte piece) {

	ArrayList<Ray> arrRay = new ArrayList<Ray>();
	int count = 0;
	boolean newRay = true;

	byte currentSquare = getSquareSouthWest(square);

	while (isValidSquare(currentSquare)) {
	    if (newRay) {
		arrRay.add(new Ray());
		count++;
		newRay = false;
	    }
	    arrRay.get(count - 1).getMoves()
		    .add(new Move(square, currentSquare));
	    currentSquare = getSquareSouthWest(currentSquare);
	}

	newRay = true;
	currentSquare = getSquareSouthEast(square);

	while (isValidSquare(currentSquare)) {
	    if (newRay) {
		arrRay.add(new Ray());
		count++;
		newRay = false;
	    }
	    arrRay.get(count - 1).getMoves()
		    .add(new Move(square, currentSquare));
	    currentSquare = getSquareSouthEast(currentSquare);
	}

	newRay = true;
	currentSquare = getSquareNorthEast(square);

	while (isValidSquare(currentSquare)) {
	    if (newRay) {
		arrRay.add(new Ray());
		count++;
		newRay = false;
	    }
	    arrRay.get(count - 1).getMoves()
		    .add(new Move(square, currentSquare));
	    currentSquare = getSquareNorthEast(currentSquare);
	}

	newRay = true;
	currentSquare = getSquareNorthWest(square);

	while (isValidSquare(currentSquare)) {
	    if (newRay) {
		arrRay.add(new Ray());
		count++;
		newRay = false;
	    }
	    arrRay.get(count - 1).getMoves()
		    .add(new Move(square, currentSquare));
	    currentSquare = getSquareNorthWest(currentSquare);
	}

	Ray[] rays = new Ray[count];

	for (int i = 0; i < count; i++) {
	    rays[i] = arrRay.get(i);
	}

	return rays;
    }

    /**
     * ROOK, all squares
     */
    private void generateRaysForRookAllSquares() {
	for (byte i = Square.A1; i <= Square.H8; i++) {

	    Ray[] rays = generateRaysForHorizontalVertical(i, PieceType.ROOK);
	    for (int k = 0; k < rays.length; k++) {
		Table.rayTable[PieceType.ROOK][i][k] = rays[k];
	    }

	    // Skip invalid square
	    if ((i + 2) % 10 == 0) {
		i += 2;
	    }
	}
    }

    /**
     * HORIZONTAL AND VERTICAL, one square
     */
    private Ray[] generateRaysForHorizontalVertical(byte square, byte piece) {

	ArrayList<Ray> arrRay = new ArrayList<Ray>();
	int count = 0;
	boolean newRay = true;

	byte currentSquare = getSquareSouth(square);

	while (isValidSquare(currentSquare)) {
	    if (newRay) {
		arrRay.add(new Ray());
		count++;
		newRay = false;
	    }
	    arrRay.get(count - 1).getMoves()
		    .add(new Move(square, currentSquare));
	    currentSquare = getSquareSouth(currentSquare);
	}

	newRay = true;
	currentSquare = getSquareNorth(square);

	while (isValidSquare(currentSquare)) {
	    if (newRay) {
		arrRay.add(new Ray());
		count++;
		newRay = false;
	    }
	    arrRay.get(count - 1).getMoves()
		    .add(new Move(square, currentSquare));
	    currentSquare = getSquareNorth(currentSquare);
	}

	newRay = true;
	currentSquare = getSquareEast(square);

	while (isValidSquare(currentSquare)) {
	    if (newRay) {
		arrRay.add(new Ray());
		count++;
		newRay = false;
	    }
	    arrRay.get(count - 1).getMoves()
		    .add(new Move(square, currentSquare));
	    currentSquare = getSquareEast(currentSquare);
	}

	newRay = true;
	currentSquare = getSquareWest(square);

	while (isValidSquare(currentSquare)) {
	    if (newRay) {
		arrRay.add(new Ray());
		count++;
		newRay = false;
	    }
	    arrRay.get(count - 1).getMoves()
		    .add(new Move(square, currentSquare));
	    currentSquare = getSquareWest(currentSquare);
	}

	Ray[] rays = new Ray[count];

	for (int i = 0; i < count; i++) {
	    rays[i] = arrRay.get(i);
	}

	return rays;
    }

    /**
     * KING, all squares
     */
    private void generateRaysForKingAllSquares() {
	for (byte i = Square.A1; i <= Square.H8; i++) {

	    Ray[] rays = generateRaysForKing(i);
	    for (int k = 0; k < rays.length; k++) {
		Table.rayTable[PieceType.KING][i][k] = rays[k];
	    }

	    // Skip invalid square
	    if ((i + 2) % 10 == 0) {
		i += 2;
	    }
	}
    }

    /**
     * KING, one square
     */
    private Ray[] generateRaysForKing(byte square) {

	ArrayList<Ray> arrRay = new ArrayList<Ray>();
	int count = 0;

	if (isValidSquare(getSquareSouth(square))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareSouth(square)));
	    count++;
	}
	if (isValidSquare(getSquareNorth(square))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareNorth(square)));
	    count++;
	}
	if (isValidSquare(getSquareWest(square))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareWest(square)));
	    // Castling rays
	    // White
	    if (square == Square.E1) {
		arrRay.get(count).getMoves().add(new Move(square, Square.G1));
		arrRay.get(count).getMoves().add(new Move(square, Square.C1));
	    }
	    // Black
	    else if (square == Square.E8) {
		arrRay.get(count).getMoves().add(new Move(square, Square.G8));
		arrRay.get(count).getMoves().add(new Move(square, Square.C8));
	    }
	    count++;
	}
	if (isValidSquare(getSquareEast(square))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareEast(square)));
	    count++;
	}
	if (isValidSquare(getSquareSouthWest(square))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareSouthWest(square)));
	    count++;
	}
	if (isValidSquare(getSquareSouthEast(square))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareSouthEast(square)));
	    count++;
	}
	if (isValidSquare(getSquareNorthWest(square))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareNorthWest(square)));
	    count++;
	}
	if (isValidSquare(getSquareNorthEast(square))) {
	    arrRay.add(new Ray());
	    arrRay.get(count).getMoves()
		    .add(new Move(square, getSquareNorthEast(square)));
	    count++;
	}

	Ray[] rays = new Ray[count];

	for (int i = 0; i < count; i++) {
	    rays[i] = arrRay.get(i);
	}

	return rays;

    }

    private byte getSquareSouth(byte square) {
	return (byte) (square - 10);
    }

    private byte getSquareNorth(byte square) {
	return (byte) (square + 10);
    }

    private byte getSquareEast(byte square) {
	return (byte) (square + 1);
    }

    private byte getSquareWest(byte square) {
	return (byte) (square - 1);
    }

    private byte getSquareSouthWest(byte square) {
	return (byte) (square - 11);
    }

    private byte getSquareSouthEast(byte square) {
	return (byte) (square - 9);
    }

    private byte getSquareNorthWest(byte square) {
	return (byte) (square + 9);
    }

    private byte getSquareNorthEast(byte square) {
	return (byte) (square + 11);
    }

    private boolean isBlackPawnStart(byte square) {
	return (square >= Square.A7) && (square <= Square.H7);
    }

    private boolean isWhitePawnStart(byte square) {
	return (square >= Square.A2) && (square <= Square.H2);
    }

    private boolean isValidSquare(byte square) {
	return (square < 80) && (square > 0) && (board[square] == Square.VALID);
    }
}
