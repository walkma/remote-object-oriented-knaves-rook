package chess.ai;

import java.util.ArrayList;
import java.util.Collections;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class AI {

    private long nrOfStaticEvalutations = 0;
    private long totalStaticEvalutations = 0;

    private int mostAccurateScore;
    private int depthSearched;
    private GameState[] gameState;
    private Node bestNode;

    static final Logger logger = LogManager.getLogger(AI.class.getName());

    public AI() {
	mostAccurateScore = Integer.MIN_VALUE;
	depthSearched = 0;
    }

    public void calcScore(GameState gameState, Move move, int desiredDepth)
	    throws Exception {

	Node root = new Node(null, null, 0);

	this.gameState = new GameState[20];
	this.gameState[0] = gameState;
	// this.gameState[1] = gameState.clone();
	// this.gameState[1].doMove(move);
	// this.gameState[1].nextPlayer();
	root.addChild(new Node(root, move, root.getDepth() + 1));

	for (int i = 2; i < desiredDepth + 1; i++) {
	    mostAccurateScore = alphaBetaMiniMax(root, Integer.MIN_VALUE,
		    Integer.MAX_VALUE, i);
	    depthSearched = i;
	    logger.info("At depth " + i);
	    totalStaticEvalutations += nrOfStaticEvalutations;
	}
	logger.info("TOTAL EVALUATIONS:     " + totalStaticEvalutations);
    }

    private void addChildrenToNode(Node node) {
	// add moves
	byte[] piecePositions;
	if (gameState[node.getDepth()].getCurrentColor() == PieceType.BLACK) {
	    piecePositions = gameState[node.getDepth()].getBlackPositions();
	} else {
	    piecePositions = gameState[node.getDepth()].getWhitePositions();
	}
	ArrayList<Move> moves = null;
	moves = MoveGenerator.generateAllMovesForPieces(piecePositions,
		gameState[node.getDepth()].getBoard());

	for (Move move : moves) {
	    node.addChild(new Node(node, move, node.getDepth() + 1));
	}

    }

    private int alphaBetaMiniMax(Node node, int alpha, int beta, int depthLimit)
	    throws Exception {
	Thread.yield();
	if (Thread.interrupted()) {
	    logger.info("TOTAL EVALUATIONS:     " + totalStaticEvalutations);
	    throw new InterruptedException();
	}

	if (node.getDepth() == depthLimit) {
	    return evaluate(node);
	}

	if (node.getParent() != null) {
	    if (!gameState[node.getDepth()].bothKingsAlive()) {
		return evaluate(node);
	    }
	    if (node.getChildren() == null) {
		addChildrenToNode(node);
	    }
	}

	ArrayList<Node> children = node.getChildren();

	// maximizing player
	if (node.getDepth() % 2 == 0) {
	    for (Node child : children) {
		// TODO: Undo move faster than clone?
		// TODO: If invalid move cancel alphabeta for this child, remove
		// child?
		if (!changeState(node, child)) {
		    continue;
		}
		int result = alphaBetaMiniMax(child, alpha, beta, depthLimit);
		if (result > alpha) {
		    alpha = result;
		    if (node.getParent() == null) {

		    }
		}
		if (alpha >= beta) {
		    node.setScore(alpha);
		    Collections.sort(node.getChildren());
		    return alpha;
		}
	    }
	    node.setScore(alpha);
	    Collections.sort(node.getChildren());
	    return alpha;

	} else {
	    // minimizing player
	    for (Node child : children) {
		// TODO: Undo move faster than clone?
		// TODO: If invalid move cancel alphabeta for this child, remove
		// child?
		if (!changeState(node, child)) {
		    continue;
		}
		int result = alphaBetaMiniMax(child, alpha, beta, depthLimit);
		if (result < beta) {
		    beta = result;
		    if (node.getParent() == null) {

		    }
		}
		if (beta <= alpha) {
		    node.setScore(beta);
		    Collections.sort(node.getChildren(),
			    Collections.reverseOrder());
		    return beta;
		}
	    }
	    node.setScore(beta);
	    Collections.sort(node.getChildren(), Collections.reverseOrder());
	    return beta;
	}
    }

    private boolean changeState(Node parent, Node child) {

	gameState[child.getDepth()] = gameState[parent.getDepth()].clone();

	if (gameState[child.getDepth()].doMove(child.getMove())) {
	    gameState[child.getDepth()].nextPlayer();
	    return true;
	} else {
	    return false;
	}
    }

    // for testing purposes
    public int calcDepth(Node root) {
	Node node = root;
	while (node.getChildren() != null) {
	    node = node.getChildren().get(0);
	}
	return node.getDepth();
    }

    public int calcNodes(Node root) {
	return getNrOfChildren(root);
    }

    private int getNrOfChildren(Node node) {
	int sum = 0;
	if (node.getChildren() != null) {
	    for (Node child : node.getChildren()) {
		sum += getNrOfChildren(child);
	    }
	} else {
	    return 1;
	}
	return sum;
    }

    public int getMostAccurate() {
	return mostAccurateScore;
    }

    public int getDepthSearched() {
	return depthSearched;
    }

    public long getTotalStaticEvalutations() {
	return totalStaticEvalutations;
    }

    public Node getBestNode() {
	return bestNode;
    }

    private int evaluate(Node node) {
	int score = BoardEvaluator.evaluate(node, gameState[node.getDepth()]);
	if (bestNode == null || bestNode.getScore() < score) {
	    bestNode = node;
	}
	nrOfStaticEvalutations++;
	node.setScore(score);
	return score;
    }
}
