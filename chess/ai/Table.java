package chess.ai;

public class Table {

    private final static byte maxPieces = 8;
    private final static byte maxSquares = 80;
    private final static byte maxRays = 8;
    public static Ray rayTable[][][];

    public static void initRayTable(){
	 rayTable = new Ray[maxPieces][maxSquares][maxRays];
	 @SuppressWarnings("unused")
	RayGenerator rayGenerator = new RayGenerator();
    }

}
