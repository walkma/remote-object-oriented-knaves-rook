package chess.ai;

public class Square {

    public final static byte L1 = 0;
    public final static byte A1 = 1;
    public final static byte B1 = 2;
    public final static byte C1 = 3;
    public final static byte D1 = 4;
    public final static byte E1 = 5;
    public final static byte F1 = 6;
    public final static byte G1 = 7;
    public final static byte H1 = 8;
    public final static byte R1 = 9;

    public final static byte L2 = 10;
    public final static byte A2 = 11;
    public final static byte B2 = 12;
    public final static byte C2 = 13;
    public final static byte D2 = 14;
    public final static byte E2 = 15;
    public final static byte F2 = 16;
    public final static byte G2 = 17;
    public final static byte H2 = 18;
    public final static byte R2 = 19;

    public final static byte L3 = 20;
    public final static byte A3 = 21;
    public final static byte B3 = 22;
    public final static byte C3 = 23;
    public final static byte D3 = 24;
    public final static byte E3 = 25;
    public final static byte F3 = 26;
    public final static byte G3 = 27;
    public final static byte H3 = 28;
    public final static byte R3 = 29;

    public final static byte L4 = 30;
    public final static byte A4 = 31;
    public final static byte B4 = 32;
    public final static byte C4 = 33;
    public final static byte D4 = 34;
    public final static byte E4 = 35;
    public final static byte F4 = 36;
    public final static byte G4 = 37;
    public final static byte H4 = 38;
    public final static byte R4 = 39;

    public final static byte L5 = 40;
    public final static byte A5 = 41;
    public final static byte B5 = 42;
    public final static byte C5 = 43;
    public final static byte D5 = 44;
    public final static byte E5 = 45;
    public final static byte F5 = 46;
    public final static byte G5 = 47;
    public final static byte H5 = 48;
    public final static byte R5 = 49;

    public final static byte L6 = 50;
    public final static byte A6 = 51;
    public final static byte B6 = 52;
    public final static byte C6 = 53;
    public final static byte D6 = 54;
    public final static byte E6 = 55;
    public final static byte F6 = 56;
    public final static byte G6 = 57;
    public final static byte H6 = 58;
    public final static byte R6 = 59;

    public final static byte L7 = 60;
    public final static byte A7 = 61;
    public final static byte B7 = 62;
    public final static byte C7 = 63;
    public final static byte D7 = 64;
    public final static byte E7 = 65;
    public final static byte F7 = 66;
    public final static byte G7 = 67;
    public final static byte H7 = 68;
    public final static byte R7 = 69;

    public final static byte L8 = 70;
    public final static byte A8 = 71;
    public final static byte B8 = 72;
    public final static byte C8 = 73;
    public final static byte D8 = 74;
    public final static byte E8 = 75;
    public final static byte F8 = 76;
    public final static byte G8 = 77;
    public final static byte H8 = 78;
    public final static byte R8 = 79;

    public final static byte VALID = 100;
    public final static byte INVALID = 101;
    public final static byte OUTOFDIMENSION = 102;

    public static byte parseString(String string) {
	string = string.toUpperCase();
	byte square = -1;
	switch (string) {
	case "A1":
	    square = A1;
	    break;
	case "A2":
	    square = A2;
	    break;
	case "A3":
	    square = A3;
	    break;
	case "A4":
	    square = A4;
	    break;
	case "A5":
	    square = A5;
	    break;
	case "A6":
	    square = A6;
	    break;
	case "A7":
	    square = A7;
	    break;
	case "A8":
	    square = A8;
	    break;
	case "B1":
	    square = B1;
	    break;
	case "B2":
	    square = B2;
	    break;
	case "B3":
	    square = B3;
	    break;
	case "B4":
	    square = B4;
	    break;
	case "B5":
	    square = B5;
	    break;
	case "B6":
	    square = B6;
	    break;
	case "B7":
	    square = B7;
	    break;
	case "B8":
	    square = B8;
	    break;
	case "C1":
	    square = C1;
	    break;
	case "C2":
	    square = C2;
	    break;
	case "C3":
	    square = C3;
	    break;
	case "C4":
	    square = C4;
	    break;
	case "C5":
	    square = C5;
	    break;
	case "C6":
	    square = C6;
	    break;
	case "C7":
	    square = C7;
	    break;
	case "C8":
	    square = C8;
	    break;
	case "D1":
	    square = D1;
	    break;
	case "D2":
	    square = D2;
	    break;
	case "D3":
	    square = D3;
	    break;
	case "D4":
	    square = D4;
	    break;
	case "D5":
	    square = D5;
	    break;
	case "D6":
	    square = D6;
	    break;
	case "D7":
	    square = D7;
	    break;
	case "D8":
	    square = D8;
	    break;
	case "E1":
	    square = E1;
	    break;
	case "E2":
	    square = E2;
	    break;
	case "E3":
	    square = E3;
	    break;
	case "E4":
	    square = E4;
	    break;
	case "E5":
	    square = E5;
	    break;
	case "E6":
	    square = E6;
	    break;
	case "E7":
	    square = E7;
	    break;
	case "E8":
	    square = E8;
	    break;
	case "F1":
	    square = F1;
	    break;
	case "F2":
	    square = F2;
	    break;
	case "F3":
	    square = F3;
	    break;
	case "F4":
	    square = F4;
	    break;
	case "F5":
	    square = F5;
	    break;
	case "F6":
	    square = F6;
	    break;
	case "F7":
	    square = F7;
	    break;
	case "F8":
	    square = F8;
	    break;
	case "G1":
	    square = G1;
	    break;
	case "G2":
	    square = G2;
	    break;
	case "G3":
	    square = G3;
	    break;
	case "G4":
	    square = G4;
	    break;
	case "G5":
	    square = G5;
	    break;
	case "G6":
	    square = G6;
	    break;
	case "G7":
	    square = G7;
	    break;
	case "G8":
	    square = G8;
	    break;
	case "H1":
	    square = H1;
	    break;
	case "H2":
	    square = H2;
	    break;
	case "H3":
	    square = H3;
	    break;
	case "H4":
	    square = H4;
	    break;
	case "H5":
	    square = H5;
	    break;
	case "H6":
	    square = H6;
	    break;
	case "H7":
	    square = H7;
	    break;
	case "H8":
	    square = H8;
	    break;
	default:
	    break;
	}
	return square;
    }

    public static String toString(byte square) {
	String string = "INVALID";
	switch (square) {
	case A1:
	    string = "A1";
	    break;
	case A2:
	    string = "A2";
	    break;
	case A3:
	    string = "A3";
	    break;
	case A4:
	    string = "A4";
	    break;
	case A5:
	    string = "A5";
	    break;
	case A6:
	    string = "A6";
	    break;
	case A7:
	    string = "A7";
	    break;
	case A8:
	    string = "A8";
	    break;
	case B1:
	    string = "B1";
	    break;
	case B2:
	    string = "B2";
	    break;
	case B3:
	    string = "B3";
	    break;
	case B4:
	    string = "B4";
	    break;
	case B5:
	    string = "B5";
	    break;
	case B6:
	    string = "B6";
	    break;
	case B7:
	    string = "B7";
	    break;
	case B8:
	    string = "B8";
	    break;
	case C1:
	    string = "C1";
	    break;
	case C2:
	    string = "C2";
	    break;
	case C3:
	    string = "C3";
	    break;
	case C4:
	    string = "C4";
	    break;
	case C5:
	    string = "C5";
	    break;
	case C6:
	    string = "C6";
	    break;
	case C7:
	    string = "C7";
	    break;
	case C8:
	    string = "C8";
	    break;
	case D1:
	    string = "D1";
	    break;
	case D2:
	    string = "D2";
	    break;
	case D3:
	    string = "D3";
	    break;
	case D4:
	    string = "D4";
	    break;
	case D5:
	    string = "D5";
	    break;
	case D6:
	    string = "D6";
	    break;
	case D7:
	    string = "D7";
	    break;
	case D8:
	    string = "D8";
	    break;
	case E1:
	    string = "E1";
	    break;
	case E2:
	    string = "E2";
	    break;
	case E3:
	    string = "E3";
	    break;
	case E4:
	    string = "E4";
	    break;
	case E5:
	    string = "E5";
	    break;
	case E6:
	    string = "E6";
	    break;
	case E7:
	    string = "E7";
	    break;
	case E8:
	    string = "E8";
	    break;
	case F1:
	    string = "F1";
	    break;
	case F2:
	    string = "F2";
	    break;
	case F3:
	    string = "F3";
	    break;
	case F4:
	    string = "F4";
	    break;
	case F5:
	    string = "F5";
	    break;
	case F6:
	    string = "F6";
	    break;
	case F7:
	    string = "F7";
	    break;
	case F8:
	    string = "F8";
	    break;
	case G1:
	    string = "G1";
	    break;
	case G2:
	    string = "G2";
	    break;
	case G3:
	    string = "G3";
	    break;
	case G4:
	    string = "G4";
	    break;
	case G5:
	    string = "G5";
	    break;
	case G6:
	    string = "G6";
	    break;
	case G7:
	    string = "G7";
	    break;
	case G8:
	    string = "G8";
	    break;
	case H1:
	    string = "H1";
	    break;
	case H2:
	    string = "H2";
	    break;
	case H3:
	    string = "H3";
	    break;
	case H4:
	    string = "H4";
	    break;
	case H5:
	    string = "H5";
	    break;
	case H6:
	    string = "H6";
	    break;
	case H7:
	    string = "H7";
	    break;
	case H8:
	    string = "H8";
	    break;
	default:
	    break;
	}
	return string;
    }
}