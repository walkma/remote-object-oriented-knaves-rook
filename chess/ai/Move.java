package chess.ai;

import java.io.Serializable;

public class Move implements Serializable {

    private static final long serialVersionUID = 1313665917184586046L;
    private byte from;
    private byte to;

    public Move(byte from, byte to) {
	this.from = from;
	this.to = to;
    }

    public byte getFrom() {
	return from;
    }

    public byte getTo() {
	return to;
    }

    public String toString(){
	return "From: " + Square.toString(from) + "\n" + "To: " + Square.toString(to) + "\n";
    }

}
