package chess.ai;

import java.util.ArrayList;

public class Node implements Comparable<Node>{

    private Node parent;
    private int depth;
    private int score;
    private Move move;

    private ArrayList<Node> children;

    public Node(Node parent, Move move, int depth){
	this.parent = parent;
	this.depth = depth;
	this.score = 0;
	this.move = move;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Node getParent(){
	return parent;
    }

    public int getDepth(){
	return depth;
    }

    public void addChild(Node node){
	if(children == null){
	    children = new ArrayList<Node>(35);
	}
	children.add(node);
    }

    public ArrayList<Node> getChildren(){
	return children;
    }

    public Move getMove() {
        return move;
    }

    @Override
    public int compareTo(Node o) {
	return o.score - score;
    }

    public String toString() {
	Node node = parent;
	String line = Square.toString(this.getMove().getFrom()) + " -> " + Square.toString(this.getMove().getTo()) + " | ";
	while(node != null && node.getMove() != null) {
	    line += Square.toString(node.getMove().getFrom()) + " -> " + Square.toString(node.getMove().getTo()) + " | ";
	    node = node.getParent();
	}

	return line;
    }



}
