package chess.ai;

public class Position {

    private byte row;
    private byte column;

    public Position(byte row, byte column){
	this.row = row;
	this.column = column;
    }

    public byte getRow() {
        return row;
    }
    public void setRow(byte row) {
        this.row = row;
    }
    public byte getColumn() {
        return column;
    }
    public void setColumn(byte column) {
        this.column = column;
    }

    public Position clone(){
	return new Position(row, column);
    }

    public boolean equals(Object o){
	if(((Position)o).column == column && ((Position)o).row == row){
	    return true;
	}
	return false;
    }

}
