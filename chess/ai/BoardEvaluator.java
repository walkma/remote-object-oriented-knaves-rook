package chess.ai;

public class BoardEvaluator {


    private final static int pawnValue = 100;
    private final static int knightValue = 320;
    private final static int bishopValue = 330;
    private final static int rookValue = 500;
    private final static int queenValue = 900;
    private final static int kingValue = 40000;
//    private final static int ownKingValue = 40000;
//    private final static int enemyKingValue = 20000;

    private final static int[] pawnPositionValues = {
	0,     0,  0,  0,  0,  0,  0,  0,  0,     0,
	0,    50, 50, 50, 50, 50, 50, 50, 50,     0,
	0,    10, 10, 20, 30, 30, 20, 10, 10,     0,
	0,     5,  5, 10, 25, 25, 10,  5,  5,     0,
	0,     0,  0,  0, 20, 20,  0,  0,  0,     0,
	0,     5, -5,-10,  0,  0,-10, -5,  5,     0,
	0,     5, 10, 10,-20,-20, 10, 10,  5,     0,
	0,     0,  0,  0,  0,  0,  0,  0,  0,     0};

    private final static int[] rookPositionValues = {
	0,     0,  0,  0,  0,  0,  0,  0,  0,     0,
	0,     5, 10, 10, 10, 10, 10, 10,  5,     0,
	0,    -5,  0,  0,  0,  0,  0,  0, -5,     0,
	0,    -5,  0,  0,  0,  0,  0,  0, -5,     0,
	0,    -5,  0,  0,  0,  0,  0,  0, -5,     0,
	0,    -5,  0,  0,  0,  0,  0,  0, -5,     0,
	0,    -5,  0,  0,  0,  0,  0,  0, -5,     0,
	0,     0,  0,  0,  5,  5,  0,  0,  0,     0};

    private final static int[] knightPositionValues = {
	0,   -50,-40,-30,-30,-30,-30,-40,-50,     0,
	0,   -40,-20,  0,  0,  0,  0,-20,-40,     0,
	0,   -30,  0, 10, 15, 15, 10,  0,-30,     0,
	0,   -30,  5, 15, 20, 20, 15,  5,-30,     0,
	0,   -30,  0, 15, 20, 20, 15,  0,-30,     0,
	0,   -30,  5, 10, 15, 15, 10,  5,-30,     0,
	0,   -40,-20,  0,  5,  5,  0,-20,-40,     0,
	0,   -50,-40,-30,-30,-30,-30,-40,-50,     0};


    private final static int[] bishopPositionValues = {
	0,   -20,-10,-10,-10,-10,-10,-10,-20,     0,
	0,   -10,  0,  0,  0,  0,  0,  0,-10,     0,
	0,   -10,  0,  5, 10, 10,  5,  0,-10,     0,
	0,   -10,  5,  5, 10, 10,  5,  5,-10,     0,
	0,   -10,  0, 10, 10, 10, 10,  0,-10,     0,
	0,   -10, 10, 10, 10, 10, 10, 10,-10,     0,
	0,   -10,  5,  0,  0,  0,  0,  5,-10,     0,
	0,   -20,-10,-10,-10,-10,-10,-10,-20,     0};


    private final static int[] whiteQueenPositionValues = {
	0,   -20,-10,-10, -5, -5,-10,-10,-20,     0,
	0,   -10,  0,  0,  0,  0,  0,  0,-10,     0,
	0,   -10,  0,  5,  5,  5,  5,  0,-10,     0,
	0,    -5,  0,  5,  5,  5,  5,  0, -5,     0,
	0,     0,  0,  5,  5,  5,  5,  0, -5,     0,
	0,   -10,  0,  5,  5,  5,  5,  5,-10,     0,
	0,   -10,  0,  0,  0,  0,  5,  0,-10,     0,
	0,   -20,-10,-10, -5, -5,-10,-10,-20,     0};

    private final static int[] blackQueenPositionValues = {
	0,   -20,-10,-10, -5, -5,-10,-10,-20,     0,
	0,   -10,  0,  0,  0,  0,  0,  0,-10,     0,
	0,   -10,  0,  5,  5,  5,  5,  0,-10,     0,
	0,    -5,  0,  5,  5,  5,  5,  0, -5,     0,
	0,     0,  0,  5,  5,  5,  5,  0, -5,     0,
	0,   -10,  5,  5,  5,  5,  5,  0,-10,     0,
	0,   -10,  0,  5,  0,  0,  0,  0,-10,     0,
	0,   -20,-10,-10, -5, -5,-10,-10,-20,     0};


    private final static int[] kingPositionValues = {
	0,   -30,-40,-40,-50,-50,-40,-40,-30,     0,
	0,   -30,-40,-40,-50,-50,-40,-40,-30,     0,
	0,   -30,-40,-40,-50,-50,-40,-40,-30,     0,
	0,   -30,-40,-40,-50,-50,-40,-40,-30,     0,
	0,   -20,-30,-30,-40,-40,-30,-30,-20,     0,
	0,   -10,-20,-20,-20,-20,-20,-20,-10,     0,
	0,    20, 20,  0,  0,  0,  0, 20, 20,     0,
	0,    20, 30, 10,  0,  0, 10, 30, 20,     0};

    public static int evaluate(Node node, GameState gameState) {
	if(gameState.getAiColor() == PieceType.BLACK){
	    return sumBlackPieces(gameState.getBlackPositions(), true) - sumWhitePieces(gameState.getWhitePositions(), false);
	} else {
	    return sumWhitePieces(gameState.getWhitePositions(), true) - sumBlackPieces(gameState.getBlackPositions(), false);
	}
    }

//    private static int sumWhitePieces(byte[] positions, boolean aiIsWhite){
//	int sum = 0;
//	for(int i = 0; i < 8; i++){
//	    if(positions[i] != Square.OUTOFDIMENSION){
//		sum += pawnValue;
//	    }
//	}
//	if(positions[8] != Square.OUTOFDIMENSION){
//	    sum += rookValue;
//	}
//	if(positions[9] != Square.OUTOFDIMENSION){
//	    sum += rookValue;
//	}
//	if(positions[10] != Square.OUTOFDIMENSION){
//	    sum += knightValue;
//	}
//	if(positions[11] != Square.OUTOFDIMENSION){
//	    sum += knightValue;
//	}
//	if(positions[12] != Square.OUTOFDIMENSION){
//	    sum += bishopValue;
//	}
//	if(positions[13] != Square.OUTOFDIMENSION){
//	    sum += bishopValue;
//	}
//	if(positions[14] != Square.OUTOFDIMENSION){
//	    sum += queenValue;
//	}
//	if(positions[15] != Square.OUTOFDIMENSION){
//	    if(aiIsWhite){
//		sum += kingValue;
//	    } else {
//		sum += kingValue;
//	    }
//	}
//
//	//extra queens
//	for(int i = 16; i < 24; i++){
//	    if(positions[i] != Square.OUTOFDIMENSION){
//
//		sum += queenValue;
//	    }
//	}
//	return sum;
//
//    }
//
//    private static int sumBlackPieces(byte[] positions, boolean aiIsBlack){
//	int sum = 0;
//	for(int i = 0; i < 8; i++){
//	    if(positions[i] != Square.OUTOFDIMENSION){
//		sum += pawnValue;
//	    }
//	}
//	if(positions[8] != Square.OUTOFDIMENSION){
//	    sum += rookValue;
//	}
//	if(positions[9] != Square.OUTOFDIMENSION){
//	    sum += rookValue;
//	}
//	if(positions[10] != Square.OUTOFDIMENSION){
//	    sum += knightValue;
//	}
//	if(positions[11] != Square.OUTOFDIMENSION){
//	    sum += knightValue;
//	}
//	if(positions[12] != Square.OUTOFDIMENSION){
//	    sum += bishopValue;
//	}
//	if(positions[13] != Square.OUTOFDIMENSION){
//	    sum += bishopValue;
//	}
//	if(positions[14] != Square.OUTOFDIMENSION){
//	    sum += queenValue;
//	}
//	if(positions[15] != Square.OUTOFDIMENSION){
//	    if(aiIsBlack){
//		sum += kingValue;
//	    } else {
//		sum += kingValue;
//	    }
//	}
//
//	//extra queens
//	for(int i = 16; i < 24; i++){
//	    if(positions[i] != Square.OUTOFDIMENSION){
//		sum += queenValue;
//	    }
//	}
//	return sum;
//
//
//    }
    private static int sumWhitePieces(byte[] positions, boolean aiIsWhite){
	int sum = 0;
	for(int i = 0; i < 8; i++){
	    if(positions[i] != Square.OUTOFDIMENSION){
		sum += pawnValue + pawnPositionValues[79-positions[i]];
	    }
	}
	if(positions[8] != Square.OUTOFDIMENSION){
	    sum += rookValue + rookPositionValues[79-positions[8]];
	}
	if(positions[9] != Square.OUTOFDIMENSION){
	    sum += rookValue + rookPositionValues[79-positions[9]];
	}
	if(positions[10] != Square.OUTOFDIMENSION){
	    sum += knightValue  + knightPositionValues[79-positions[10]];
	}
	if(positions[11] != Square.OUTOFDIMENSION){
	    sum += knightValue  + knightPositionValues[79-positions[11]];
	}
	if(positions[12] != Square.OUTOFDIMENSION){
	    sum += bishopValue  + bishopPositionValues[79-positions[12]];
	}
	if(positions[13] != Square.OUTOFDIMENSION){
	    sum += bishopValue  + bishopPositionValues[79-positions[13]];
	}
	if(positions[14] != Square.OUTOFDIMENSION){
	    sum += queenValue + whiteQueenPositionValues[79-positions[14]];
	}
	if(positions[15] != Square.OUTOFDIMENSION){
	    if(aiIsWhite){
		sum += kingValue + kingPositionValues[79-positions[15]];
	    } else {
		sum += kingValue + kingPositionValues[79-positions[15]];
	    }
	}

	//extra queens
	for(int i = 16; i < 24; i++){
	    if(positions[i] != Square.OUTOFDIMENSION){

		sum += queenValue + whiteQueenPositionValues[79-positions[i]];
	    }
	}
	return sum;

    }

    private static int sumBlackPieces(byte[] positions, boolean aiIsBlack){
	int sum = 0;
	for(int i = 0; i < 8; i++){
	    if(positions[i] != Square.OUTOFDIMENSION){
		sum += pawnValue + pawnPositionValues[positions[i]];
	    }
	}
	if(positions[8] != Square.OUTOFDIMENSION){
	    sum += rookValue + rookPositionValues[positions[8]];
	}
	if(positions[9] != Square.OUTOFDIMENSION){
	    sum += rookValue + rookPositionValues[positions[9]];
	}
	if(positions[10] != Square.OUTOFDIMENSION){
	    sum += knightValue  + knightPositionValues[positions[10]];
	}
	if(positions[11] != Square.OUTOFDIMENSION){
	    sum += knightValue  + knightPositionValues[positions[11]];
	}
	if(positions[12] != Square.OUTOFDIMENSION){
	    sum += bishopValue  + bishopPositionValues[positions[12]];
	}
	if(positions[13] != Square.OUTOFDIMENSION){
	    sum += bishopValue  + bishopPositionValues[positions[13]];
	}
	if(positions[14] != Square.OUTOFDIMENSION){
	    sum += queenValue + blackQueenPositionValues[positions[14]];
	}
	if(positions[15] != Square.OUTOFDIMENSION){
	    if(aiIsBlack){
		sum += kingValue + kingPositionValues[positions[15]];;
	    } else {
		sum += kingValue + kingPositionValues[positions[15]];;
	    }
	}

	//extra queens
	for(int i = 16; i < 24; i++){
	    if(positions[i] != Square.OUTOFDIMENSION){
		sum += queenValue + blackQueenPositionValues[positions[i]];
	    }
	}
	return sum;


    }

}
