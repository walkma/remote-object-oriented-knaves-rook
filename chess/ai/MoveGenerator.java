package chess.ai;

import java.util.ArrayList;

public class MoveGenerator {


    // TODO list of pieces or not? hard to update, but may be better than
    // looping through entire board?
    public static ArrayList<Move> generateAllMovesForPieces(byte[] piecePositions,
	    byte[] board) {
	ArrayList<Move> moves = new ArrayList<Move>(100);
	byte piece;
	for (int i = 0; i < piecePositions.length; i++) {
	    if (piecePositions[i] != Square.OUTOFDIMENSION) {

		piece = board[piecePositions[i]];

		generateMovesForPiece(PieceType.getPieceType(piece), piece,
			piecePositions[i], board, moves);

	    }
	}
	return moves;
    }

    /**
     *
     * @param pieceType
     *            - example PieceType.ROOK
     * @param piece
     *            - example PieceType.BLACK_ROOK
     * @param square
     *            - the square where the piece is standing
     * @param board
     *            - the chess board with all alive pieces
     * @param allMoves
     *            - the vector which valid moves are added to
     */
    private static void generateMovesForPiece(byte pieceType, byte piece, byte square,
	    byte[] board, ArrayList<Move> allMoves) {
	Ray ray;
	for (int i = 0; i < 8; i++) {
	    if(pieceType == -1 || square == -1) {
		System.out.println("Square: " + square);
		System.out.println("PieceType: " + pieceType);
		System.out.println("Piece: " + piece);
		System.out.println("Color: " + PieceType.isBlack(piece));
	    }
	    ray = Table.rayTable[pieceType][square][i];
	    if (ray == null) {
		break;
	    }
	    for (Move move : ray.getMoves()) {

		if (isEmpty(move.getTo(), board)) {
		    if(isValidForPawn(pieceType, move, board)) {
			allMoves.add(move);
		    }
		} else {
		    // can easily separate between attacking moves and silent
		    // moves here!!!
		    if (isAttacking(piece, move.getTo(), board)) {
			if(isValidForPawn(pieceType, move, board)) {
			    allMoves.add(move);
			}
		    }
		    break;
		}

	    }
	}
    }

    /**
     *
     * @param square
     *            - the square the piece is trying to move to
     * @param board
     *            - the chess board
     * @return - true if the square is empty otherwise false
     */
    private static boolean isEmpty(byte square, byte[] board) {
	if (board[square] == PieceType.NONE) {
	    return true;
	}
	return false;
    }

    /**
     *
     * @param piece
     *            - example PieceType.BLACK_ROOK
     * @param square
     *            - the square the piece is moving to
     * @param board
     *            - the board with all the pieces
     * @return true if attacking (enemy piece on the square)
     */
    public static boolean isAttacking(byte piece, byte square, byte[] board) {
	if (PieceType.isBlack(piece)) {
	    if (PieceType.isWhite(board[square])) {
		return true;
	    }
	} else if (PieceType.isWhite(piece)) {
	    if (PieceType.isBlack(board[square])) {
		return true;
	    }
	}
	return false;

    }

    private static boolean isValidForPawn(byte pieceType, Move move, byte[] board) {

	int diff = Math.abs(move.getFrom() - move.getTo());
	if(pieceType == PieceType.BLACK_PAWN || pieceType == PieceType.WHITE_PAWN) {
	    if (diff == 9 || diff == 11) {
		return !isEmpty(move.getTo(), board);
	    } else {
		return isEmpty(move.getTo(), board);
	    }
	} else {
	    return true;
	}


    }

}
