package chess.data;

import java.util.ArrayList;
import java.util.List;

import chess.ai.Move;

import rook.data.Context;
import rook.data.Task;
import rook.data.TaskResult;
import rook.data.Work;
import rook.data.WorkResult;

public class ChessWork extends Work {

    private static final long serialVersionUID = 5387896099396534590L;
    ArrayList<Move> moves;

    public ChessWork(Context context, ArrayList<Move> moves) {
	super(context);
	this.moves = moves;
    }

    @Override
    public List<Task> split(int pieces) {
	ArrayList<Task> tasks = new ArrayList<Task>(moves.size());
	for(Move move: moves){
	    tasks.add(new ChessTask(move, this.getMaxTime()));
	}
	return tasks;
    }

    @Override
    public WorkResult merge(List<TaskResult> taskResults, int totalTasks) {

	int bestScore = Integer.MIN_VALUE;
	int score = 0;
	int depth = 0;
	Move move = null;
	for(TaskResult chessResult: taskResults){
	    score = ((ChessTaskResult)chessResult).getScore();
	    System.out.println("Score in chesswork: " + score + " Depth searched: " + ((ChessTaskResult)chessResult).getDepth() + " Move: " + ((ChessTaskResult)chessResult).getMove());
	    if( score > bestScore){
		move = ((ChessTaskResult)chessResult).getMove();
		bestScore = score;
		depth = ((ChessTaskResult)chessResult).getDepth();
	    }
	}
	WorkResult workResult = new ChessWorkResult(move, bestScore, depth, totalTasks, taskResults.size());
	return workResult;
    }


}
