package chess.data;

import chess.ai.Move;
import rook.data.WorkResult;

public class ChessWorkResult extends WorkResult{

    private static final long serialVersionUID = -8599140890436008978L;
    private Move bestMove;
    private int score;
    private int depth;



    public ChessWorkResult(Move bestMove, int score, int depth, int totalTasks, int completedTasks){
	super(totalTasks, completedTasks);
	this.bestMove = bestMove;
	this.score = score;
	this.depth = depth;
    }

    public Move getBestMove() {
        return bestMove;
    }

    public int getScore() {
        return score;
    }

    public int getDepth() {
        return depth;
    }



}
