package chess.data;

import chess.ai.GameState;
import rook.data.Context;

public class ChessContext implements Context {

    private static final long serialVersionUID = -8704082191831481398L;
    private GameState gameState;
    private int desiredDepth;

    public ChessContext(GameState gameState, int desiredDepth) {
	this.gameState = gameState;
	this.desiredDepth = desiredDepth;
    }

    public GameState getGameState() {
        return gameState;
    }

    public int getDesiredDepth() {
        return desiredDepth;
    }
}
