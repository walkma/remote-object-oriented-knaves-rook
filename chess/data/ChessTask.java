package chess.data;

import chess.ai.Move;
import rook.data.Task;

public class ChessTask extends Task {

    private static final long serialVersionUID = 334445195334662027L;
    private Move move;

    public ChessTask(Move move, int maxTime) {
	super(maxTime);
	this.move = move;
    }

    public Move getMove() {
	return move;
    }


}
