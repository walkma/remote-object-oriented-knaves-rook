package chess.data;

import java.util.List;

import chess.ai.Move;

import rook.data.TaskResult;

public class ChessTaskResult extends TaskResult {

    private static final long serialVersionUID = -5914282448938049915L;
    private int score;
    private int depth;
    private Move move;

    public ChessTaskResult(int taskID, int workID, int score, int depth, Move move, int performance) {
	super(taskID, workID, performance);
	this.score = score;
	this.depth = depth;
	this.move = move;
    }

    @Override
    public TaskResult pickBestResult(List<TaskResult> resultList,
	    int replication, int quorum) {
	TaskResult retVal = null;
	int deepestDepth = - 1;
	int depth = -1;
	for(TaskResult chessResult: resultList){
	    depth = ((ChessTaskResult)chessResult).getDepth();
	    if(depth > deepestDepth){
		deepestDepth = depth;
		retVal = chessResult;
	    }
	}
	return retVal;
    }

    public int getScore() {
	return score;
    }

    public int getDepth() {
	return depth;
    }

    public Move getMove() {
	return move;
    }

}
