package chess.server;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import rook.communication.ServerCom;
import rook.data.Work;
import rook.data.WorkResult;
import rook.server.RookServer;
import chess.ai.GameState;
import chess.ai.Move;
import chess.ai.MoveGenerator;
import chess.ai.PieceType;
import chess.ai.Square;
import chess.ai.Table;
import chess.data.ChessContext;
import chess.data.ChessWork;
import chess.data.ChessWorkResult;

//TODO: Change so maxTime is used in server and client
public class ChessServer extends RookServer{
    private GameState gameState;
    private GameState backupState;
    private int desiredDepth;

    static final Logger logger = LogManager.getLogger(ChessServer.class.getName());

    public ChessServer(String aiColor, String propertyFile){
	super();
	byte color = -1;
	if(aiColor.equals("black")){
	    color = PieceType.BLACK;
	} else if(aiColor.equals("white")){
	    color = PieceType.WHITE;
	}

	//Read properties
	String propertyFileName = (propertyFile != null) ? propertyFile
		: "src/client.properties";
	Properties prop = new Properties();
	try {
	    FileInputStream in = new FileInputStream(propertyFileName);
	    prop.load(in);
	    in.close();
	} catch (IOException e) {
	    logger.error("Reading properties file: " + propertyFileName
		    + " failed");
	    logger.error(e.getMessage());
	    return;
	}
	desiredDepth = Integer.parseInt(prop.getProperty("desiredDepth", "7"));

	gameState = new GameState(color);
	Table.initRayTable();
    }

    public void doMove(String from, String to){
	Move move = new Move(Square.parseString(from), Square.parseString(to));
	backupState = gameState.clone();
	gameState.doMove(move);
    }

    public Move doAIMove(){
	Work work = null;
	byte[] piecePositions = null;

	if(gameState.getCurrentColor() == PieceType.BLACK){
	    piecePositions = gameState.getBlackPositions();
	} else if(gameState.getCurrentColor() == PieceType.WHITE){
	    piecePositions = gameState.getWhitePositions();
	}
	ArrayList<Move> moves = MoveGenerator.generateAllMovesForPieces(piecePositions, gameState.getBoard());
	System.out.println("nr of moves: " + moves.size());
	work = new ChessWork(new ChessContext(gameState, desiredDepth), moves);
	WorkResult workResult = doWork(work);
	if(workResult == null){
	    //no moves returned
	    gameState = backupState;
	    return null;
	}
	System.out.println("Score for move: " + ((ChessWorkResult)workResult).getScore());
	if(((ChessWorkResult)workResult).getBestMove() == null){
	    return null;
	}
	gameState.doMove(((ChessWorkResult)workResult).getBestMove());
	return ((ChessWorkResult)workResult).getBestMove();
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public ServerCom getServerCom() {
	return this.serverCom;
    }

}
