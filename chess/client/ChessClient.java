package chess.client;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import chess.ai.AI;
import chess.ai.Table;
import chess.data.ChessContext;
import chess.data.ChessTaskResult;
import chess.data.ChessTask;
import rook.client.RookClient;
import rook.data.Context;
import rook.data.Task;
import rook.data.TaskResult;

public class ChessClient extends RookClient {

    static final Logger logger = LogManager.getLogger(ChessClient.class.getName());

    public ChessClient(String propertyFile) {
	super(propertyFile);
	Table.initRayTable();
    }

    @Override
    protected TaskResult doTask(final Task task, final Context context) {
	final AI ai = new AI();
	Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
	Thread t = new Thread(new Runnable(){

	    @Override
	    public void run() {
		    try {
			Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
			ai.calcScore(((ChessContext)context).getGameState(), ((ChessTask)task).getMove(), ((ChessContext)context).getDesiredDepth());
			logger.info(InetAddress.getLocalHost().getHostName() + " is returning normally.");
		    } catch (Exception e) {
			try {
			    logger.info(InetAddress.getLocalHost().getHostName() + " is returning from exception " + e.toString());
			    e.printStackTrace();
			    //e.printStackTrace();
			} catch (UnknownHostException e1) {
			    logger.error("Unknown host");
			}
		    }
	    }

	});
	t.start();
	long startTime = System.currentTimeMillis();
	for(int i = 0; i < ((ChessTask)task).getMaxTime(); i++){
	    try {
		if(((System.currentTimeMillis() / 1000) - (startTime / 1000)) > ((ChessTask)task).getMaxTime()){
		    logger.info("Client " + InetAddress.getLocalHost().getHostName() + " recieved timeout.");
		    break;
		}
		TimeUnit.SECONDS.sleep(1);
	    } catch (InterruptedException | UnknownHostException e) {
		e.printStackTrace();
	    }
	    if(!t.isAlive()) {
		logger.info("The thread is dead");
		break;
	    }
	}
	t.interrupt();
	try {
	    logger.info(ai.getBestNode());
	    logger.info("Client " + InetAddress.getLocalHost().getHostName() + " is returning the result.");
	    logger.info(((System.currentTimeMillis() / 1000) - (startTime / 1000)));
	} catch (UnknownHostException e) {
	    logger.error("Unknown host");
	}
	//System.gc();


	return new ChessTaskResult(task.getTaskID(), task.getWorkID(), ai.getMostAccurate(), ai.getDepthSearched(), ((ChessTask)task).getMove(), (int)ai.getTotalStaticEvalutations());
    }





}
