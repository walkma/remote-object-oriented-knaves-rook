package chess.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

import chess.ai.GameState;
import chess.ai.Move;
import chess.ai.PieceType;
import chess.server.ChessServer;

public class MainChessServer {

    private static ChessServer chessServer;
    private static LinkedList<GameState> turns = new LinkedList<GameState>();

    public static void main(String[] args) throws IOException,
	    InterruptedException {


	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	String line = "";
	String aiColor = "";
	System.out.print("Choose AI color: ");
	aiColor = br.readLine();
	if (!aiColor.equals("black") && !aiColor.equals("white")) {
	    aiColor = "black";
	}
	chessServer = new ChessServer(aiColor, "src/chess.properties");
	System.out.println("Waitning for clients to join..");
	// TODO: Wait for a certain amount of clients?

	System.out.println("Press enter to start the game, or wait for more clients to join.");
	br.readLine();
	System.out.println();
	if (aiColor.equals("white")) {
	    turns.push(chessServer.getGameState().clone());
	    if(!doAIMove()){
		undoMove();
	    }
	}

	System.out.println(chessServer.getGameState());

	while (!line.equals("quit")) {
	    System.out.print("Enter your move: ");
	    line = br.readLine();
	    System.out.println();
	    if(line.equals("restart")) {
		byte color = 0;
		if(aiColor.equals("black")){
		    color = PieceType.BLACK;
		} else if(aiColor.equals("white")){
		    color = PieceType.WHITE;
		}
		chessServer.setGameState(new GameState(color));
		if(aiColor.equals("white")) {
		    doAIMove();
		}
		System.out.println(chessServer.getGameState());
		turns.clear();
		turns.push(chessServer.getGameState().clone());
	    } else if(line.equals("undo")) {
		undoMove();
	    } else {
		String move[] = line.split(" ");

		if(move.length > 2 || move.length < 2) {
		    System.err.println("Enter origin and destination square.");
		    continue;
		} else if((!move[0].matches("[a-hA-H][1-8]") || !move[1].matches("[a-hA-H][1-8]")) || move[0].equals(move[1])) {
		    System.err.println("Invalid move.");
		    continue;
		}
		turns.push(chessServer.getGameState().clone());
		chessServer.doMove(move[0], move[1]);
		long startTime = System.currentTimeMillis();
		if(!doAIMove()){
		    undoMove();
		} else {
		    System.out.println("Execution time: " + ((System.currentTimeMillis() / 1000) - (startTime / 1000)) + " seconds.");
		    System.out.println(chessServer.getGameState());
		}
	    }
	}
    }

    private static boolean doAIMove() {
	Move madeMove = chessServer.doAIMove();
	if (madeMove == null) {
	    System.out
		    .println("Unfortunately no move was returned from the clients");
	    return false;
	} else {
	    System.out.println(madeMove);
	}
	return true;
    }

    private static void undoMove(){
	if(turns.isEmpty()){
	    return;
	}
	chessServer.setGameState(turns.poll());
	System.out.println(chessServer.getGameState());
    }
}
