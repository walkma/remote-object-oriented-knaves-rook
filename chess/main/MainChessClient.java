package chess.main;

import java.io.IOException;

import chess.client.ChessClient;

public class MainChessClient {
    public static void main(String[] args) throws IOException {
	ChessClient chessClient = new ChessClient("src/client.properties");
	chessClient.startWorking();
    }
}
