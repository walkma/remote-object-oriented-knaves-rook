package rook.server;

import java.io.FileInputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rook.communication.ReceiveInterface;
import rook.communication.ServerCom;
import rook.data.Context;
import rook.data.Task;
import rook.data.TaskResult;
import rook.data.Work;
import rook.data.WorkResult;
import rook.message.AddTimeMessage;
import rook.message.ContextMessage;
import rook.message.ResultMessage;
import rook.message.TaskMessage;


public abstract class RookServer {

    private int port;
    protected ServerCom serverCom;
    private Registry registry;

    private int executionTimeLimit;
    private int totalTimeLimit;
    private int timeIncrement;

    private int replication;
    private int quorum;

    private int taskHistory;

    public RookServer() {
	this("src/server.properties");
    }

    public RookServer(String propertyFile) {
	// Load settings
	String propertyFileName = (propertyFile != null) ? propertyFile : "src/server.properties";
	Properties prop = new Properties();
	try {
	    FileInputStream in = new FileInputStream(propertyFileName);
	    prop.load(in);
	    in.close();
	} catch (IOException e) {
	    System.err.println("Reading properties file: " + propertyFileName + " failed");
	    System.err.println(e.getMessage());
	    return;
	}
	port = Integer.parseInt(prop.getProperty("port", "1099"));
	executionTimeLimit = Integer.parseInt(prop.getProperty("executionTimeLimit", "10"));
	totalTimeLimit = Integer.parseInt(prop.getProperty("totalTimeLimit", "20"));
	timeIncrement = Integer.parseInt(prop.getProperty("timeIncrement", "10"));
	replication = Integer.parseInt(prop.getProperty("replication", "3"));
	quorum = Integer.parseInt(prop.getProperty("quorum", "2"));
	taskHistory = Integer.parseInt(prop.getProperty("taskHistory", "15"));

	try {
	    // Create RMI registry
	    LocateRegistry.createRegistry(port);

	    // Create ServerReceive stub
	    serverCom = new ServerCom(taskHistory);
	    ReceiveInterface serverStub = (ReceiveInterface) UnicastRemoteObject.exportObject(serverCom, 0);


	    // Bind stub
	    registry = LocateRegistry.getRegistry(port);
	    registry.rebind("server", serverStub);
	} catch (RemoteException e) {
	    System.err.println("Setting up RMI failed");
	    System.err.println(e.getMessage());
	    return;
	}
    }

    public WorkResult doWork(Work work) {
	work.setMaxTime(executionTimeLimit);
	ArrayList<ClientPointer> clientList = new ArrayList<ClientPointer>(serverCom.getClients());
	try {
	    System.out.println("ClientList size = " + clientList.size());
	    for (int i = 0; i < clientList.size(); i++) {
		System.out.println("Client " + i + ": " + clientList.get(i));
	    }
	    Collections.sort(clientList);
	} catch (NullPointerException e) {
	    System.err.println("null pointer... wtf?!");
	    e.printStackTrace();
	}
	ArrayList<Task> taskList = new ArrayList<Task>(work.split(clientList.size()));
	LinkedList<Task> taskQueue = new LinkedList<Task>();
	LinkedList<ClientPointer> clientQueue = new LinkedList<ClientPointer>();
	Context context = work.getContext();
	int nrOfTasks = taskList.size();
	double totalPerformance = 0;
	long startTime = System.currentTimeMillis();

	// Duplicate tasks replication times and shuffle the list
	Collections.shuffle(taskList);
	for (int j = 0; j < replication; j++) {
	    for (int i = 0; i < nrOfTasks; i++) {
		taskList.get(i).setID(i);
		taskQueue.add(taskList.get(i));
	    }
	}

	// Send Context message to all clients
	// TODO Decide to use either a separate remove list (active) or just get the new client list after (commented) (the if should still be there)
	// Risk with getting the new list: if a client joins after the loop but before the get, we get a list with clients that may not have a context.
	ArrayList<ClientPointer> removeClientList = new ArrayList<ClientPointer>();
	for (ClientPointer c : clientList) {
	    // TODO Send message should return a bool indicating if it succeeded (to be able to remove failed clients)
	    if (serverCom.sendMessage(c, new ContextMessage(context))) {
		totalPerformance += c.getAvgPerformance();
		clientQueue.add(c);
	    } else {
		removeClientList.add(c);
	    }
	}
	clientList.removeAll(removeClientList);
	//	clientList = new ArrayList<ClientPointer>(serverCom.getClients());

	// Make sure the result queue is empty before starting the next job
	serverCom.getMessageQueue().clear();

	// Error if there are less than "replication" number of clients active when computation starts
	if (clientList.size() < replication) {
	    System.err.println("Replication is set to " + replication + " when only " + clientList.size() + "clients are responding.");
	    return null;
	}

	for (ClientPointer c : clientList) {
	    c.setTotalPerformanceFraction(c.getAvgPerformance() / totalPerformance);
	}

	// Distribute tasks depending on previous performance, task load and replication
	// TODO Prevent poisoning by distributing tasks more randomly
	// TODO Distribute tasks depending on client performance
	// number of tasks depending on percentage of total performance? cap on percentage to prevent starvation?
	// This solution: Randomly re-add clients to the queue depending on the fraction of the total work they do.
	// this way clients with high performance get re-added often
	// but doesn't check if a client gets multiple copies of the same task (TODO check this...)
	while (!taskQueue.isEmpty()) {
	    serverCom.sendMessage(clientQueue.poll(), new TaskMessage(taskQueue.poll()));
	    // TODO start timer for this task in this client, remember to stop and recalculate when the result arrives
	    for (ClientPointer c : clientList) {
		//TODO changed to 1 for testing
		if (chance(1)) {
		    clientQueue.add(c);
		}
	    }
	}

	// Wait for task results from all clients, timeout before time is up
	ArrayList<TaskResult> taskResultList;
	int elapsedTime = ((int) (System.currentTimeMillis() - startTime)) / 1000;
	int taskResultCount = 0;
	TaskResult[][] replicatedResults = new TaskResult[nrOfTasks][replication];

	ResultMessage m = null;
	boolean timeout = false;
	for (taskResultCount = 0; taskResultCount < (nrOfTasks * replication); taskResultCount++) {
	    try {
//		m = (ResultMessage) serverCom.getMessageQueue().take();
		m = (ResultMessage) serverCom.getMessageQueue().poll((int) (totalTimeLimit - elapsedTime), TimeUnit.SECONDS);
//		System.out.println("TAKE RESPONSE");
	    } catch (InterruptedException e) {
		System.err.println("Waiting for results interrupted by other cause");
		System.err.println(e.getMessage());
		timeout = true;
		break;
	    }

	    if (m != null) {
		TaskResult tr = m.getResult();

		// Make sure the TaskResult is valid by checking it against the replicated tasks
		// TODO do we really trust the clients to create good IDs? Yes, we do.
		for (int i = 0; i < replication; i++) {
		    if (replicatedResults[tr.getTaskID()][i] == null) {
			replicatedResults[tr.getTaskID()][i] = tr;
		    }
		}
		// TODO kinda important. Update client information (performance and shit)
	    } else {
		timeout = true;
		break;
	    }
	}

	taskResultList = pickBestResults(replicatedResults, nrOfTasks);

	if (timeout) {
	    System.err.println("ERROR TIMEOUT");
//	    System.err.println("Timeout! Only " + taskResultList.size() + " / " + nrOfTasks + " tasks were completed in time. (" + taskResultCount + " / " + nrOfTasks + " task results)");
	}

	if(taskResultList == null) {
	    return null;
	} else {
	    return work.merge(taskResultList, nrOfTasks);
	}

    }

    private ArrayList<TaskResult> pickBestResults(TaskResult[][] replicatedResults, int nrOfTasks) {
	ArrayList<TaskResult> taskResultList = new ArrayList<TaskResult>();
	int completedTasks = 0;

	for (int i = 0; i < nrOfTasks; i++) {
	    ArrayList<TaskResult> partialTaskResultList = new ArrayList<TaskResult>();
	    for (int j = 0; j < replication; j++) {
		if (replicatedResults[i][j] != null) {
		    partialTaskResultList.add(replicatedResults[i][j]);
		}
	    }
	    try {
		taskResultList.add(partialTaskResultList.get(0).pickBestResult(partialTaskResultList, replication, quorum));
		completedTasks += 1;
	    } catch (IndexOutOfBoundsException e) {
		// TODO remove unnecessary  completed task counter? Only used for error message...
		System.err.println("No results were returned for task: " + i + ". Completion rate reduced to: " + completedTasks / (i + 1));
	    }
	}
	return taskResultList;
    }

    private boolean chance(double probability) {
	if ((probability > 1) || (probability < 0)) {
	    System.err.println("Client performance can't exceed 100%");
	    return false;
	}
	int roll = new Random().nextInt(1000);
	return (roll <= (probability * 1000));
    }

    public void incrementTime() {
	incrementTime(timeIncrement);
    }

    public void incrementTime(int timeIncrement) {
	ArrayList<ClientPointer> clientList = new ArrayList<ClientPointer>(serverCom.getClients());
	for (ClientPointer cp : clientList) {
	    serverCom.sendMessage(cp, new AddTimeMessage(timeIncrement));
	}
    }
}
