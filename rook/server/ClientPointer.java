package rook.server;

import java.rmi.Remote;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Stack;

import rook.communication.ReceiveInterface;
import rook.data.Task;
import rook.data.TaskResult;


public class ClientPointer implements Comparable<ClientPointer> {

    private ReceiveInterface clientStub;
    private static int IDCounter = 0;

    private final int ID;
    private long timer;
    private int nrOfCompletedJobs;
    private double avgPerformance;
    private double totalPerformanceFraction;
    private double recentPerformance;

    private HashMap<Integer, Task> currentTasks;

    // TODO List past of performances to take changes / trends into account?
    Stack<TaskPerformance> taskHistory;
    private int taskHistorySize;

    public ClientPointer(ReceiveInterface clientStub, int taskHistorySize) {
	this.ID = IDCounter++;
	this.clientStub = clientStub;
	this.nrOfCompletedJobs = 0;
	this.avgPerformance = 0;
	this.totalPerformanceFraction = 0;
	this.recentPerformance = 0;
	this.taskHistory = new Stack<TaskPerformance>();
	this.taskHistorySize = taskHistorySize;
    }

    public void registerTask(Task t) {
	currentTasks = new HashMap<Integer, Task>();
	currentTasks.put(t.getTaskID(), t);
	timer = System.currentTimeMillis();
    }

    public void taskCompleted(TaskResult result, double performance) {
	// TODO Find a good way of calculating performance
	long timeSpent = (System.currentTimeMillis() - timer);
	// Performance per second
	performance /= (timeSpent * 1000);

	// Calculate total performance
	double totalPerformance = (avgPerformance * nrOfCompletedJobs);
	nrOfCompletedJobs++;
	totalPerformance += performance;
	avgPerformance = (totalPerformance / nrOfCompletedJobs);

	// TODO Calculate recent performance
	// Add task to history
	taskHistory.push(new TaskPerformance(result.getWorkID(), result.getTaskID(), result.getPerformance(), timeSpent));
	// Get the top taskHistorySize number of tasks from the stack

	// Calculate average performance
    }

    public int getNrOfCompletedJobs() {
	return nrOfCompletedJobs;
    }

    public double getAvgPerformance() {
	return avgPerformance;
    }

    public Remote stub() {
	return clientStub;
    }

    public double getTotalPerformanceFraction() {
	return totalPerformanceFraction;
    }

    public void setTotalPerformanceFraction(double totalPerformanceFraction) {
	this.totalPerformanceFraction = totalPerformanceFraction;
    }

    public int compareTo(ClientPointer c) {
	// TODO Find a good way of comparing performances (percentages of avg and change?)
	return (int) (Math.signum(avgPerformance - c.getAvgPerformance()));
    }

    public String toString() {
	return ("" + ID);
    }
}
