package rook.server;

public class TaskPerformance {
    private int workID;
    private int taskID;
    private double performance;
    private long executionTime;
    
    // TODO Maybe make this abstract and let the user implement a measure of performance (instead of a simple double)
    public TaskPerformance(int workID, int taskID, double performance, long executionTime) {
	this.workID = workID;
	this.taskID = taskID;
	this.performance = performance;
	this.executionTime = executionTime;
    }
    
    public int getWorkID() {
	return workID;
    }
    
    public int getTaskID() {
	return taskID;
    }
    
    public double getPerformance() {
	return performance;
    }
    
    public long getExecutionTime() {
	return executionTime;
    }
}
