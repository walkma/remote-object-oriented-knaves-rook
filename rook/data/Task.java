package rook.data;

import java.io.Serializable;

// TODO comparable is only required if the server needs to check if a client gets the same task twice
public abstract class Task implements Serializable /*extends Comparable<Task>*/ {
    private static final long serialVersionUID = 6840027982262546363L;
    private /*final*/ int workID;
    private /*final*/ int taskID;
    private int maxTime;

    public Task(int maxTime){
	this.maxTime = maxTime;
    }

    public void setMaxTime(int maxTime) {
	this.maxTime = maxTime;
    }

    public int getMaxTime() {
	return maxTime;
    }

    public int getWorkID() {
	return workID;
    }

    public int getTaskID() {
	return taskID;
    }

    // TODO remove
    public void setID(int ID) {
	this.taskID = ID;
    }

}
