package rook.data;

import java.io.Serializable;
import java.util.List;

public abstract class TaskResult implements Serializable {
    //TODO: Remove task? only ID should be needed

    private static final long serialVersionUID = -6367222599937667828L;
    private final int taskID;
    private final int workID;
    private int performance;


    /**
     * This method would be static if Java wasn't retarded
     * warning: resultList can be null if no results have been returned.
     *
     * @param resultList (can be null)
     * @param replication
     * @param quorum
     * @return
     */
    public abstract TaskResult pickBestResult(List<TaskResult> resultList, int replication, int quorum);


    public TaskResult(int taskID, int workID, int performance) {
	this.taskID = taskID;
	this.workID = workID;
	this.performance = performance;
    }

    public int getTaskID() {
	return taskID;
    }

    public int getWorkID() {
	return workID;
    }

    public int getPerformance() {
	return performance;
    }
}
