package rook.data;

import java.io.Serializable;

public abstract class WorkResult implements Serializable {
    private static final long serialVersionUID = 8271276694603182816L;
    private int totalTasks;
    private int completedTasks;

    public WorkResult(int totalTasks, int completedTasks) {
	this.totalTasks = totalTasks;
	this.completedTasks = completedTasks;
    }

    public int getTotalTasks() {
	return totalTasks;
    }

    public int getCompletedTasks() {
	return completedTasks;
    }

    public double getCompletionRate() {
	return (double) (completedTasks / totalTasks);
    }
}
