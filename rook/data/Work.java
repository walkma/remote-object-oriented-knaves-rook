package rook.data;

import java.io.Serializable;
import java.util.List;

public abstract class Work implements Serializable {

    private static final long serialVersionUID = 7133611642044449913L;
    private Context context;
    protected int maxTime;

    public Work(Context context) {
	this.context = context;
    }

    public void setMaxTime(int maxTime) {
	this.maxTime = maxTime;
    }

    public int getMaxTime() {
	return maxTime;
    }

    public abstract List<Task> split(int pieces);
    public abstract WorkResult merge(List<TaskResult> taskResults, int totalTasks);
    public Context getContext(){
	return context;
    }
}
