package rook.message;

import rook.data.Task;

public class TaskMessage implements Message {

    private static final long serialVersionUID = 7466441928217603647L;
    private Task task;

    public TaskMessage(Task task) {
	this.task = task;
    }

    public Task getTask() {
	return task;
    }
    
    public String toString() {
	return "TaskMessage";
    }
}
