package rook.message;

import rook.data.Context;

public class ContextMessage implements Message {

    private static final long serialVersionUID = -4655105186779149765L;
    private Context context;

    public ContextMessage(Context context) {
	this.context = context;
    }

    public Context getContext() {
	return context;
    }
    
    public String toString() {
	return "ContextMessage";
    }
}
