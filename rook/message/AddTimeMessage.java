package rook.message;

public class AddTimeMessage implements Message {

    private static final long serialVersionUID = -3564066131862145774L;
    private int time;

    public AddTimeMessage(int time) {
	this.time = time;
    }

    public int getTime() {
	return time;
    }

    public String toString() {
	return "AddTimeMessage (time = " + time + ")";
    }
}
