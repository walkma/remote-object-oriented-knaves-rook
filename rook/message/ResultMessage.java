package rook.message;

import rook.data.TaskResult;

public class ResultMessage implements Message {

    private static final long serialVersionUID = -1209705797924585463L;
    private TaskResult result;

    public ResultMessage(TaskResult result) {
	this.result = result;
    }

    public TaskResult getResult() {
        return result;
    }
    
    public String toString() {
	return "ResultMessage";
    }
}
