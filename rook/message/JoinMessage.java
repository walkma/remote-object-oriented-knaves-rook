package rook.message;

import rook.communication.ReceiveInterface;

public class JoinMessage implements Message {

    private static final long serialVersionUID = 4142957190637944908L;
    private ReceiveInterface stub;

    public JoinMessage(ReceiveInterface stub) {
	this.stub = stub;
    }

    public ReceiveInterface getStub() {
        return stub;
    }
    
    public String toString() {
	return "JoinMessage";
    }
}
