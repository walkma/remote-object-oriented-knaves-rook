package rook.communication;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import rook.message.JoinMessage;
import rook.message.Message;
import rook.message.ResultMessage;
import rook.server.ClientPointer;


public class ServerCom implements ReceiveInterface {

    private LinkedBlockingQueue<Message> messageQueue;
    private List<ClientPointer> clients;
    private int taskHistory;

    public LinkedBlockingQueue<Message> getMessageQueue() {
	return messageQueue;
    }

    public List<ClientPointer> getClients() {
	return clients;
    }

    public ServerCom(int taskHistory) {
	messageQueue = new LinkedBlockingQueue<Message>();
	clients = Collections.synchronizedList(new ArrayList<ClientPointer>());
	this.taskHistory = taskHistory;
    }

    @Override
    public void receiveMessage(Message message) throws RemoteException {
	try {
	    processMessage(message);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
    }

    public boolean sendMessage(ClientPointer cp, Message message) {
	try {

	    if(cp == null){
		System.out.println("STUB IS NULLL");
	    }

	    ((ReceiveInterface) cp.stub()).receiveMessage(message);
	} catch (RemoteException e) {
	    System.err.println("Sending " + message + " to client " + cp + " failed. Removing client");
	    clients.remove(cp);
	    return false;
	}
	return true;
    }

    private void processMessage(Message message) throws InterruptedException {
	if (message instanceof ResultMessage) {
	    messageQueue.put(message);
	} else if (message instanceof JoinMessage) {
	    System.out.println("CLIENT JOIN (" + clients.size() + ")");
	    clients.add(new ClientPointer(((JoinMessage) message).getStub(), taskHistory));
	}
    }

    public int getTaskHistory() {
	return taskHistory;
    }

    public void setTaskHistory(int taskHistory) {
	this.taskHistory = taskHistory;
    }
}
