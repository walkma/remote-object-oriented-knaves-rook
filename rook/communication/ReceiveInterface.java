package rook.communication;

import java.rmi.Remote;
import java.rmi.RemoteException;

import rook.message.Message;


public interface ReceiveInterface extends Remote {
    public void receiveMessage(Message message) throws RemoteException;
}
