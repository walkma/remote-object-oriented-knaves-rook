package rook.communication;

import java.rmi.RemoteException;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import rook.message.Message;

public class ClientCom implements ReceiveInterface {

    static final Logger logger = LogManager.getLogger(ClientCom.class.getName());

    private LinkedBlockingQueue<Message> messageQueue;
    private ReceiveInterface serverStub;

    public ClientCom(ReceiveInterface serverStub) {
	messageQueue = new LinkedBlockingQueue<Message>();
	this.serverStub = serverStub;
    }

    @Override
    public void receiveMessage(Message message) throws RemoteException {
	try {
	    processMessage(message);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
    }

    public void sendMessage(Message message) {
	try {
	    serverStub.receiveMessage(message);
	} catch (RemoteException e) {
	    // TODO: Retry, exit?
	    e.printStackTrace();
	}
    }

    private void processMessage(Message message) throws InterruptedException {
	messageQueue.put(message);
    }

    public LinkedBlockingQueue<Message> getMessageQueue() {
        return messageQueue;
    }
}
