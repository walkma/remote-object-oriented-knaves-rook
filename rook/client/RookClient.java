package rook.client;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import rook.communication.ClientCom;
import rook.communication.ReceiveInterface;
import rook.data.Context;
import rook.data.Task;
import rook.data.TaskResult;
import rook.message.ContextMessage;
import rook.message.JoinMessage;
import rook.message.Message;
import rook.message.ResultMessage;
import rook.message.TaskMessage;

public abstract class RookClient {

    static final Logger logger = LogManager.getLogger(RookClient.class.getName());

    private ClientCom clientCom;
    private Context context;
    private ConcurrentHashMap<Integer, Thread> taskThreads;

    public RookClient(String propertyFile) {
	taskThreads = new ConcurrentHashMap<Integer, Thread>();

	//Read properties
	String propertyFileName = (propertyFile != null) ? propertyFile
		: "src/client.properties";
	Properties prop = new Properties();
	try {
	    FileInputStream in = new FileInputStream(propertyFileName);
	    prop.load(in);
	    in.close();
	} catch (IOException e) {
	    logger.error("Reading properties file: " + propertyFileName
		    + " failed");
	    logger.error(e.getMessage());
	    return;
	}
	int port = Integer.parseInt(prop.getProperty("port", "1099"));
	String host = prop.getProperty("host", "localhost");

	//Get server stub and send join message
	ReceiveInterface serverStub;
	try {
	    serverStub = (ReceiveInterface) LocateRegistry.getRegistry(host,
	    	port).lookup("server");
	    clientCom = new ClientCom(serverStub);

	    ReceiveInterface clientStub = (ReceiveInterface) UnicastRemoteObject
		    .exportObject(clientCom, 0);
	    logger.info("Sending Join from " + InetAddress.getLocalHost().getHostName());
	    clientCom.sendMessage(new JoinMessage(clientStub));
	} catch (AccessException e) {
	    logger.error("AccessException: " + e.getMessage());
	} catch (RemoteException e) {
	    logger.error("RemoteException: " + e.getMessage());
	} catch (NotBoundException e) {
	    logger.error("NotBoundException: " + e.getMessage());
	} catch (UnknownHostException e) {
	    logger.error("UnknownHostException: " + e.getMessage());
	}
    }

    public void startWorking() {
	while (true) {
	    try {
		Message message = clientCom.getMessageQueue().take();

		if (message instanceof ContextMessage) {
		    ContextMessage contextMessage = (ContextMessage) message;
		    context = contextMessage.getContext();
		    // ABORT any ongoing calculations
		    logger.info(InetAddress.getLocalHost().getHostName() + " recieved ContextMessage.");
		    for(Thread t: taskThreads.values()) {
			t.interrupt();
		    }
		    taskThreads.clear();
		} else if (message instanceof TaskMessage) {
		    Thread taskThread = new Thread(new TaskWorker((TaskMessage)message));
		    taskThreads.put(((TaskMessage)message).getTask().getTaskID(), taskThread);
		    taskThread.start();
		}
	    } catch (InterruptedException | UnknownHostException e) {
		logger.error("InterruptedException: " + e.getMessage());
	    }
	}
    }

    private class TaskWorker implements Runnable {

	private TaskMessage taskMessage;

	public TaskWorker(TaskMessage taskMessage) {
	    this.taskMessage = taskMessage;
	}

	@Override
	public void run() {
	    TaskResult result = doTask(taskMessage.getTask(), context);
	    clientCom.sendMessage(new ResultMessage(result));
	    taskThreads.remove(result.getTaskID());
	}

    }

    protected abstract TaskResult doTask(Task task, Context context);
}
