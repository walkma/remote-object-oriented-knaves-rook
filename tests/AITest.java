package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import chess.ai.AI;
import chess.ai.GameState;
import chess.ai.Move;
import chess.ai.PieceType;
import chess.ai.Square;
import chess.ai.Table;

public class AITest {

    private AI ai;
    private GameState gameState;

    @Before
    public void setUp() throws Exception {
	ai = new AI();
	Table.initRayTable();
	gameState = new GameState(PieceType.WHITE);
    }

    @Test
    public void test() throws Exception {

	ai.calcScore(gameState, new Move(Square.B2, Square.B3), 3);

	System.out.println("DONE");
	assertTrue(true);
    }

}
