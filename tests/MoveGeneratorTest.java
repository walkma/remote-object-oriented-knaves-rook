package tests;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import chess.ai.ChessBoardBuilder;
import chess.ai.GameState;
import chess.ai.Move;
import chess.ai.MoveGenerator;
import chess.ai.PieceType;
import chess.ai.Square;
import chess.ai.Table;

public class MoveGeneratorTest {

    byte[] board;
    byte[] white;
    byte[] black;
    MoveGenerator moveGenerator;
    GameState gameState;

    @Before
    public void setUp() throws Exception {
	board = ChessBoardBuilder.buildChessBoard();
	white = ChessBoardBuilder.buildWhitePositions();
	black = ChessBoardBuilder.buildBlackPositions();
	moveGenerator = new MoveGenerator();
	gameState = new GameState(PieceType.BLACK);
	Table.initRayTable();

    }

    @Test
    public void testGenerateMovesForPawn() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
	//private void generateMovesForPiece(byte pieceType, byte piece, byte square, byte[] board, ArrayList<Move> allMoves)
	ArrayList<Move> moves = new ArrayList<Move>(20);
	Method method =
		MoveGenerator.class.getDeclaredMethod("generateMovesForPiece", byte.class, byte.class, byte.class, byte[].class, ArrayList.class);
		// Make it accessible
		method.setAccessible(true);
		// Test the method
		method.invoke(moveGenerator, PieceType.WHITE_PAWN, PieceType.WHITE_PAWN, Square.B2, board, moves);
		assertEquals(2, moves.size());

		moves.clear();

		method.invoke(moveGenerator, PieceType.BLACK_PAWN, PieceType.BLACK_PAWN, Square.H7, board, moves);

		assertEquals(2,moves.size());

		moves.clear();

		method.invoke(moveGenerator, PieceType.BLACK_PAWN, PieceType.BLACK_PAWN, Square.H5, board, moves);

		assertEquals(1,moves.size());

		moves.clear();

		method.invoke(moveGenerator, PieceType.BLACK_PAWN, PieceType.BLACK_PAWN, Square.B3, board, moves);

		assertEquals(2,moves.size());

		moves.clear();

		method.invoke(moveGenerator, PieceType.WHITE_PAWN, PieceType.WHITE_PAWN, Square.H6, board, moves);

		assertEquals(1,moves.size());

		moves.clear();

		gameState.doMove(new Move(Square.B2, Square.B3));
		gameState.doMove(new Move(Square.D2, Square.D3));
		gameState.doMove(new Move(Square.B1, Square.C3));
		method.invoke(moveGenerator, PieceType.WHITE_PAWN, PieceType.WHITE_PAWN, Square.C2, gameState.getBoard(), moves);


		assertEquals(0,moves.size());
    }

    @Test
    public void testGenerateMovesForKing() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
	ArrayList<Move> moves = new ArrayList<Move>(20);
	Method method =
		MoveGenerator.class.getDeclaredMethod("generateMovesForPiece", byte.class, byte.class, byte.class, byte[].class, ArrayList.class);
	// Make it accessible
	method.setAccessible(true);

	//test king that cant walk
	method.invoke(moveGenerator, PieceType.KING, PieceType.BLACK_KING, Square.E8, board, moves);

	assertEquals(0, moves.size());

	moves.clear();

	//test king that can walk all directions
	method.invoke(moveGenerator, PieceType.KING, PieceType.WHITE_KING, Square.E4, board, moves);
	assertEquals(moves.size(),8);

	moves.clear();

	//test the white king close to own pieces
	method.invoke(moveGenerator, PieceType.KING, PieceType.WHITE_KING, Square.E3, board, moves);
	assertEquals(moves.size(),5);

	moves.clear();

	//test the white king close to black pieces
	method.invoke(moveGenerator, PieceType.KING, PieceType.WHITE_KING, Square.A6, board, moves);
	assertEquals(moves.size(),5);

    }

    @Test
    public void testGenerateMovesForRook() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
	ArrayList<Move> moves = new ArrayList<Move>(20);
	Method method =
		MoveGenerator.class.getDeclaredMethod("generateMovesForPiece", byte.class, byte.class, byte.class, byte[].class, ArrayList.class);
	// Make it accessible
	method.setAccessible(true);

	//test white rook on C4
	method.invoke(moveGenerator, PieceType.ROOK, PieceType.WHITE_ROOK, Square.C4, board, moves);
	assertEquals(moves.size(), 11);

	moves.clear();

	//test black rook on A6
	method.invoke(moveGenerator, PieceType.ROOK, PieceType.BLACK_ROOK, Square.A6, board, moves);
	assertEquals(moves.size(), 11);

	moves.clear();

	//test white rook on H8 (in black base)
	method.invoke(moveGenerator, PieceType.ROOK, PieceType.WHITE_ROOK, Square.H8, board, moves);
	assertEquals(moves.size(), 2);

    }

    @Test
    public void testGenerateMovesForBishop() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
	ArrayList<Move> moves = new ArrayList<Move>(20);
	Method method =
		MoveGenerator.class.getDeclaredMethod("generateMovesForPiece", byte.class, byte.class, byte.class, byte[].class, ArrayList.class);
	// Make it accessible
	method.setAccessible(true);

	//test white bishop on E4
	method.invoke(moveGenerator, PieceType.BISHOP, PieceType.WHITE_BISHOP, Square.E4, board, moves);
	assertEquals(moves.size(), 8);

	moves.clear();

	//test black bishop on F3
	method.invoke(moveGenerator, PieceType.BISHOP, PieceType.BLACK_BISHOP, Square.F3, board, moves);
	assertEquals(moves.size(), 7);

    }

    @Test
    public void testGenerateMovesForQueen() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
	ArrayList<Move> moves = new ArrayList<Move>(20);
	Method method =
		MoveGenerator.class.getDeclaredMethod("generateMovesForPiece", byte.class, byte.class, byte.class, byte[].class, ArrayList.class);
	// Make it accessible
	method.setAccessible(true);

	//test white queen on E8
	method.invoke(moveGenerator, PieceType.QUEEN, PieceType.WHITE_QUEEN, Square.E8, board, moves);
	assertEquals(moves.size(), 5);

	moves.clear();

	//test black queen on C5
	method.invoke(moveGenerator, PieceType.QUEEN, PieceType.BLACK_QUEEN, Square.C5, board, moves);
	assertEquals(moves.size(), 18);

    }

    @Test
    public void testGenerateMovesForKnight() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
	ArrayList<Move> moves = new ArrayList<Move>(20);
	Method method =
		MoveGenerator.class.getDeclaredMethod("generateMovesForPiece", byte.class, byte.class, byte.class, byte[].class, ArrayList.class);
	// Make it accessible
	method.setAccessible(true);

	//test white knight on G1
	method.invoke(moveGenerator, PieceType.KNIGHT, PieceType.WHITE_KNIGHT, Square.G1, board, moves);
	assertEquals(moves.size(), 2);

	moves.clear();

	//test black knight on A4
	method.invoke(moveGenerator, PieceType.KNIGHT, PieceType.BLACK_KNIGHT, Square.A4, board, moves);

	assertEquals(moves.size(), 4);

	moves.clear();

	method.invoke(moveGenerator, PieceType.KNIGHT, PieceType.WHITE_KNIGHT, Square.B1, board, moves);
	assertEquals(moves.size(), 2);




    }

    @SuppressWarnings("unchecked")
    @Test
    public void testGenerateMovesForWhitePieces() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {


	ArrayList<Move> moves = new ArrayList<Move>(50);
	Method method =
		MoveGenerator.class.getDeclaredMethod("generateAllMovesForPieces", byte[].class, byte[].class);
	// Make it accessible
	method.setAccessible(true);

	moves = (ArrayList<Move>) method.invoke(moveGenerator, white, board);
	assertEquals(20, moves.size());
    }

}
