package tests;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import chess.ai.AI;
import chess.ai.Node;

public class TreeTest {

    private AI ai;

    @Before
    public void setUp() throws Exception {
	ai = new AI();
    }

    @Test
    public void testTree() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {


	Node root = new Node(null, null, 0);

	assertEquals(0, ai.calcDepth(root));
	assertEquals(1, ai.calcNodes(root));


	Method method =
		AI.class.getDeclaredMethod("addDepth", Node.class);
	// Make it accessible
	method.setAccessible(true);

	method.invoke(ai, root);

	assertEquals(1, ai.calcDepth(root));
	assertEquals(20, ai.calcNodes(root));

	method.invoke(ai, root);

	assertEquals(2, ai.calcDepth(root));
	assertEquals(400, ai.calcNodes(root));

	method.invoke(ai, root);

	assertEquals(3, ai.calcDepth(root));
	assertEquals(8902, ai.calcNodes(root));

	method.invoke(ai, root);

	assertEquals(4, ai.calcDepth(root));
	assertEquals(197742, ai.calcNodes(root));
	method.invoke(ai, root);

    }

}
