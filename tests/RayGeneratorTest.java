package tests;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Before;
import org.junit.Test;

import chess.ai.PieceType;
import chess.ai.Square;
import chess.ai.Table;

public class RayGeneratorTest {


    @Before
    public void setUp() throws Exception {
	Table.initRayTable();
    }

    @Test
    public void testKingMoves() {
	assertThat(Table.rayTable[PieceType.KING][Square.H1][1].getMoves().size(), is(1));
	assertNull(Table.rayTable[PieceType.KING][Square.H1][4]);
    }

    @Test
    public void testRookMoves() {
	assertThat(Table.rayTable[PieceType.ROOK][Square.H1][1].getMoves().size(), is(7));
	assertNull(Table.rayTable[PieceType.ROOK][Square.H1][4]);
    }

    @Test
    public void testKnightMoves() {
	assertThat(Table.rayTable[PieceType.KNIGHT][Square.H1][0].getMoves().size(), is(1));
	assertThat(Table.rayTable[PieceType.KNIGHT][Square.H1][1].getMoves().size(), is(1));
	assertNull(Table.rayTable[PieceType.KNIGHT][Square.H1][2]);
    }

    @Test
    public void testQueenMoves() {
	assertThat(Table.rayTable[PieceType.QUEEN][Square.H1][0].getMoves().size(), is(7));
	assertThat(Table.rayTable[PieceType.QUEEN][Square.H1][1].getMoves().size(), is(7));
	assertNull(Table.rayTable[PieceType.QUEEN][Square.H1][3]);
    }

    @Test
    public void testBlackPawnMoves() {
	assertThat(Table.rayTable[PieceType.BLACK_PAWN][Square.B7][0].getMoves().size(), is(2));
	assertNull(Table.rayTable[PieceType.BLACK_PAWN][Square.B7][3]);
	assertThat(Table.rayTable[PieceType.BLACK_PAWN][Square.B6][0].getMoves().size(), is(1));
	assertNull(Table.rayTable[PieceType.BLACK_PAWN][Square.B6][3]);
    }

    @Test
    public void testWhitePawnMoves() {
	assertThat(Table.rayTable[PieceType.WHITE_PAWN][Square.B2][0].getMoves().size(), is(2));
	assertNull(Table.rayTable[PieceType.WHITE_PAWN][Square.B2][3]);
	assertThat(Table.rayTable[PieceType.WHITE_PAWN][Square.B3][0].getMoves().size(), is(1));
	assertNull(Table.rayTable[PieceType.WHITE_PAWN][Square.B3][3]);
    }

    @Test
    public void testBishopMoves() {
	assertThat(Table.rayTable[PieceType.BISHOP][Square.A8][0].getMoves().size(), is(7));
	assertNull(Table.rayTable[PieceType.BISHOP][Square.A8][1]);
	assertThat(Table.rayTable[PieceType.BISHOP][Square.D4][0].getMoves().size(), is(3));
    }

}
