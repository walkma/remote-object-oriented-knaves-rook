package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

import chess.ai.Node;

public class NodeTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testNodeCompare() {
	ArrayList<Node> nodes = new ArrayList<Node>();
	Node node1 = new Node(null, null, 0);
	node1.setScore(1);
	Node node2 = new Node(null, null, 0);
	node2.setScore(5);
	Node node3 = new Node(null, null, 0);
	node3.setScore(-43);
	nodes.add(node3);
	nodes.add(node2);
	nodes.add(node1);

	Collections.sort(nodes);

	assertEquals(5,nodes.get(0).getScore());


    }

}
